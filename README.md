To compile Siraela, you can either use cmake or call the compiler directly. If you do not have C++20 compiler,
it is possible to compile it as C++17, but it is not a preferred choice for the compiler.

To execute simulation, e.g. with patients and scenarios from 
https://gitlab.com/sirael_metabolic_simulator/diabetes-type-1-subjects/

use the siraela.exe aka Siraela Terminal. To compile source metabolic code to the Sirael IR,
use the sc.exe aka Sirael Language Compiler.

To learn more, se:
 Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024). 
 Available at https://link.springer.com/article/10.1007/s44174-024-00199-9 or https://doi.org/10.1007/s44174-024-00199-9 