#
# Sirael - in-silico patient model
# Copyright (c) since 2020 Tomas Koutny.
#
# Contact:
# tomas@sirael.science
# Tomas Koutny
#
#
# Purpose of this software:
# This software is a personal, research project of Tomas Koutny. It is strictly
# prohibited to use this software for diagnosis or treatment of any medical condition,
# without proving a safety for the intended use.
#
# By using this software, you are responsible for:
#  a) implementing all safety requirements,
#  b) proving the safety for a particular purpose,
#  c) obtaining all regulatory approvals,
#  d) any arising harm.
#
# Especially, a diabetic patient is warned that improper use of this software
# may result into severe injure, including death.
#
#
#
# Licensing terms:
# This software is granted under the Apache 2.0 license for a non-commercial use.
# This software can be granted for a commercial use under a written agreement only.
# Always, you must include and display this header-comment when redistributing the software in any form.
#
# When referring to this work, use the following citation:
# Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
# Available at
#  https://link.springer.com/article/10.1007/s44174-024-00199-9
#  or https://doi.org/10.1007/s44174-024-00199-9
#  or git: https://gitlab.com/sirael_metabolic_simulator/
#

#
# Parts of the string-conversion methods of the GUID class were adopted from stringutils.cpp 
# of the Apache-v2.0-licensed SmartCGMS SDK:
#    Tomas Koutny and Martin Ubl, "SmartCGMS as a Testbed for a Blood-Glucose Level Prediction and/or 
#    Control Challenge with (an FDA-Accepted) Diabetic Patient Simulation", Procedia Computer Science,  
#    Volume 177, pp. 354-362, 2020
#


import siraela_lib
import sys


class Siraela_Replay(object):

    vm = siraela_lib.Siraela_VM()

    def __init__(self, dll_path, ir_path):
        self.vm.load_lib(dll_path)
        self.vm.load_IR(ir_path)
        

    def command(self, command_line):
        self.vm.command(command_line, self.signal_callback)
 
    def signal_callback(self, id, level):
        if id==siraela_lib.Siraela_VM.simulated_cgm_sensor:                        
            print("Simulated CGM sensor glucose level: " + str(level))
        elif id==siraela_lib.Siraela_VM.simulated_glycemia:
            print("Simulated blood glucose level: " + str(level))
        else:
            print(str(id) + " : " + str(level))
            



if len(sys.argv) != 5:
    sys.stdout.write('Usage: py sirael.py path_to_siraela_dll metabolism.sir_file scenario_to_replay state_index\n')
    sys.stdout.flush()
else:
    simulation = Siraela_Replay(sys.argv[1], sys.argv[2]) 
    simulation.command("reset " + sys.argv[4])
    commands = open(sys.argv[3], "r")
    for line in commands:
        simulation.command(line)