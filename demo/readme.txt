To execute this example, it has to reside in the very same directory as the siraela.exe 
In addition, you need to provide metabolism and scenario description files. You can download them freely
from the repository, or you can alter this example to load them from a custom location.

The metabolism file describe patient's metabolism and some states, indexed from zero. There is at least one state.
A state represents the patient's state at some given time with substances in the bodily fluids forming a valid,
physiogically plausible state.

Scenario file represents a schedule, when the patient ingested food, what activity took place, etc. For your convenience,
the prepared scenario files also contain instructions for advancing and sampling the simulation. Once you are
familiar with the Siraela terminal, you can chnage them as you see fit.