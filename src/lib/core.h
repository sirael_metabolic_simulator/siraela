/**
 * Sirael - in-silico patient model
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */


#include <sirael/lib/sirael_iface.h>

#include <sirael/lang_parser/sirael_lang_adt.h>
#include <sirael/vm/siraela_vm.h>

#include <optional>
#include <string>

class CCore {
protected:
	const sirael_lang::TSirael_Program mLoaded_Program;
	std::unique_ptr<CSiraela_VM> mVM;
protected:
	uint64_t mWall_Clock_Seconds = 0;
	uint64_t mJitter = 0;
	uint64_t mStepping = std::numeric_limits<decltype(mStepping)>::max();
	double mInsulin_Units_Per_Stepping = 0.0;
public:
	CCore(sirael_lang::TSirael_Program& program);
	uint64_t State_Count();
	std::tuple<uint64_t, std::string> Select_State(const size_t state_index);
	
	uint64_t Step(uint64_t seconds);

	void Set_Insulin_Basal_Rate(const double sbr);
	bool Try_Consume(const GUID& id, const double quantity);

	uint64_t Sampled_Level_Count();
	uint64_t Sample_Levels(GUID* const ids, double* levels);
};
