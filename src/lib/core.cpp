/**
 * Sirael - in-silico patient model
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */


#include "core.h"


CCore::CCore(sirael_lang::TSirael_Program& program) : mLoaded_Program(program) {

}


uint64_t CCore::State_Count() {
	return mLoaded_Program.states.size();
}

std::tuple<uint64_t, std::string> CCore::Select_State(const size_t state_index) {
	if (state_index >= mLoaded_Program.states.size())
		return { std::numeric_limits<uint64_t>::max(), "Index out of bounds" };
	
	mVM.reset();
	mWall_Clock_Seconds = 0;
	mJitter = 0;
	mStepping = std::numeric_limits<decltype(mStepping)>::max();
	mInsulin_Units_Per_Stepping = 0.0;
	

	std::unique_ptr<CSiraela_VM> new_vm = std::make_unique<CSiraela_VM>();
	if (new_vm->Initialize(mLoaded_Program, state_index)) {
		mVM = std::move(new_vm);
		mStepping = mLoaded_Program.usecs_stepping / (1000 * 1000);	//convert from microseconds to seconds
		return { state_index, "" };
	}
	else
		return { std::numeric_limits<uint64_t>::max(), "Error: Failed to initialize Siraela VM!" };

	return { std::numeric_limits<uint64_t>::max(), "Program control-flow has failed!" };	//it should never get here
}

uint64_t CCore::Step(uint64_t seconds) {
	if (!mVM)
		return std::numeric_limits<uint64_t>::max();

	const auto start = mWall_Clock_Seconds;

	do {
		const auto desired_time = mWall_Clock_Seconds + seconds + mJitter;//note that Sirael advances by stepping, so it stops at or before the desired time

		while (mWall_Clock_Seconds + mStepping <= desired_time) {
			bool ok = mInsulin_Units_Per_Stepping == 0.0;

			if (!ok)
				ok = mVM->Try_Consume(scgms::signal_Requested_Insulin_Bolus, mInsulin_Units_Per_Stepping);

			if (!ok) {				
				return mWall_Clock_Seconds - start;
			}

			if (mVM->Step())
				mWall_Clock_Seconds += mStepping;
			else {				
				return mWall_Clock_Seconds - start;
			}
		}

		mJitter = desired_time - mWall_Clock_Seconds;
		seconds = 0;	//we have already processed the requested seconds
	} while (mJitter >= mStepping);

	const auto stop = mWall_Clock_Seconds;
	return stop - start;
}

void CCore::Set_Insulin_Basal_Rate(const double sbr) {
	mInsulin_Units_Per_Stepping = static_cast<double>(mStepping) * sbr / static_cast<double>(60 * 60);	//units per hour => convert to units per second	
}

bool CCore::Try_Consume(const GUID& id, const double quantity) {
	if (!mVM)
		return false;

	return mVM->Try_Consume(id, quantity);
}

uint64_t CCore::Sampled_Level_Count() {	
	size_t output_signal_count = 0;
	for (const auto& sig : mLoaded_Program.signals)
		if (sig.flags && sirael_lang::NSignal_Flag::emit)
			output_signal_count++;

	return output_signal_count;
}

uint64_t CCore::Sample_Levels(GUID* const ids, double* levels) {
	if (!mVM)
		return 0;

	uint64_t output_signal_id = 0;
	mVM->Emit_Signals([&output_signal_id, &ids, &levels](const GUID& id, double value)->bool {
		ids[output_signal_id] = id;
		levels[output_signal_id] = value;
		output_signal_id++;
		return true;
	});


	return output_signal_id;
}