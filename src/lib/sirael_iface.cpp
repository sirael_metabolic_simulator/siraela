/**
 * Sirael - in-silico patient model
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */


#include "core.h"

#include <sirael/lang_parser/sirael_ir_reader.h>

#include <limits>

#undef min

void Copy_String(const std::string& src, char* const dst_buffer, const size_t dst_buffer_size) {
	if (!src.empty() && (dst_buffer_size > 0)) {
		const size_t max_to_copy = std::min(dst_buffer_size - 1, src.size());
		const char* raw_msg = src.data();
		memcpy(dst_buffer, raw_msg, max_to_copy);
		dst_buffer[max_to_copy] = 0;
	}
}

template <typename... Args>
TSirael_Handle Load_Metabolism_Core(char* const error_buffer, const size_t error_buffer_size, const Args... args) {
	TSirael_Handle result = Invalid_Sirael_Handle;
	*error_buffer = 0;

	//load the Sirael IR 
	auto ir = Read_Program_IR(args...);

	//copy any error message or a warning
	Copy_String(ir.error_msg, error_buffer, error_buffer_size);
	
	if (ir.valid) {
		//create the object if IR seems OK
		CCore* core = new CCore(ir.program);
		result = reinterpret_cast<void*>(core);
	}

	return result;
}

TSirael_Handle C_Calling Load_Metabolism_From_File(const char* IR_file_path, char* const error_buffer, const size_t error_buffer_size) {
	return Load_Metabolism_Core(error_buffer, error_buffer_size, IR_file_path);
}

TSirael_Handle C_Calling Load_Metabolism_From_Memory(const void* IR_buffer, const size_t buffer_size, char* const error_buffer, const size_t error_buffer_size) {
	return Load_Metabolism_Core(error_buffer, error_buffer_size, error_buffer, error_buffer_size);
}

void C_Calling Free_Metabolism(TSirael_Handle handle) {
	if (handle != Invalid_Sirael_Handle) {
		CCore* core = reinterpret_cast<CCore*>(handle);
		delete core;
	}
}

uint64_t C_Calling Get_State_Count(TSirael_Handle handle) {
	if (handle != Invalid_Sirael_Handle) {
		CCore* core = reinterpret_cast<CCore*>(handle);
		return core->State_Count();
	}
	else
		return std::numeric_limits<uint64_t>::max();
}


uint64_t C_Calling Select_Metabolism_State(TSirael_Handle handle, const uint64_t state_index, char* const error_buffer, const size_t error_buffer_size) {
	uint64_t result = std::numeric_limits<uint64_t>::max();;

	if (handle != Invalid_Sirael_Handle) {
		CCore* core = reinterpret_cast<CCore*>(handle);
		const auto [selected_index, err_msg] = core->Select_State(state_index);
		result = selected_index;
		Copy_String(err_msg, error_buffer, error_buffer_size);
	} else {
		Copy_String("Invalid handle!", error_buffer, error_buffer_size);
	}

	return result;
}

uint64_t C_Calling Step(TSirael_Handle handle, const uint64_t seconds) {
	uint64_t result = std::numeric_limits<uint64_t>::max();;

	if (handle != Invalid_Sirael_Handle) {
		CCore* core = reinterpret_cast<CCore*>(handle);
		result = core->Step(seconds);
	}

	return result;
}

uint64_t C_Calling Try_Consume(TSirael_Handle handle, const GUID* id, double quantity) {
	uint64_t result = std::numeric_limits<uint64_t>::max();;

	if (handle != Invalid_Sirael_Handle) {
		CCore* core = reinterpret_cast<CCore*>(handle);
		result = static_cast<uint64_t>(core->Try_Consume(*id, quantity));
	}

	return result;
}


uint64_t C_Calling Sampled_Level_Count(TSirael_Handle handle) {
	uint64_t result = std::numeric_limits<uint64_t>::max();;

	if (handle != Invalid_Sirael_Handle) {
		CCore* core = reinterpret_cast<CCore*>(handle);
		result = core->Sampled_Level_Count();
	}

	return result;
}

uint64_t C_Calling Sample_Levels(TSirael_Handle handle, GUID* const ids, double* levels) {
	uint64_t result = std::numeric_limits<uint64_t>::max();;

	if (handle != Invalid_Sirael_Handle) {
		CCore* core = reinterpret_cast<CCore*>(handle);
		result = core->Sample_Levels(ids, levels);
	}

	return result;
}