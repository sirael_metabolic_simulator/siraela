 /**
 * Sirael Language Compiler
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */

/*
 * Why is there no parser generator used?
 * 1. Do not want to pollute the project with Java, e.g.; when using Antlr
 * 2. Yacc/Bison/Flex do not produce a code that withstands static analysis across multiple platforms
 * 3. Boost's Spirit is a hell to debug
 */


#include "sirael_lang_parser.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

#include <scgms/utils/string_utils.h>

const std::string dev_null_str = "dev_null";
const std::string init_instr_str = "init:";

/* declared in string utils
bool ends_with(std::string const& str, std::string const& ending) {
	if (ending.size() > str.size()) 
		return false;

	return std::equal(ending.rbegin(), ending.rend(), str.rbegin());
}
*/
bool begins_with(std::string const& str, std::string const& beginning) {
	if (beginning.size() > str.size())
		return false;

	return std::equal(beginning.begin(), beginning.end(), str.begin());
}


std::string remove_line_comments(const std::string& str) {
	std::string result;
	bool could_be_comment_begin = false;
	bool could_be_block_comment_end = false;
	bool in_block_comment = false;
	for (size_t i = 0; i < str.size(); i++) {

		char ch = str[i];
		if (ch == '\t')	//unify the white spaces
			ch = ' ';

		if (in_block_comment) {
			if (could_be_block_comment_end) {
				if (ch == '/') {
					could_be_block_comment_end = false;
					could_be_comment_begin = false;
					in_block_comment = false;
				}
			}
			else {
				if (ch == '*') {
					could_be_block_comment_end = true;
					could_be_comment_begin = false;
				}
			}
		}
		else {
			if (could_be_comment_begin) {
				if (ch == '/')
					return result;	//we reach eol C++ comment
				else if (ch == '*')
					in_block_comment = true;
				else {
					result += '/';		//append the holding comment-possible char
					result += ch;	//append the current char
					could_be_comment_begin = false;
				}
			}
			else {
				if (ch == '/')
					could_be_comment_begin = true;
				else
					result += ch;
			}
		}
	}

	return result;
}

std::vector<sirael_lang::TRaw_Section> Pre_Process_Source(const filesystem::path src_path) {
	std::vector<sirael_lang::TRaw_Section> result;

	std::fstream src_file {src_path};
	if (src_file.is_open()) {		
		std::string line;

		bool in_section = false;
		size_t line_counter = 0;
		sirael_lang::TRaw_Section section;
		
		const std::string begin_str = "begin";
		const std::string end_str = ".end";

		while (std::getline(src_file, line)) {
			line_counter++;

			line = trim(line);
			if (line.empty() || begins_with(line, "//"))
				continue;

			if (in_section) {
				//check section end				
				if (begins_with(line, end_str)) {
					const auto found_name = trim(line.substr(end_str.size()));
					if (found_name == section.name) {
						result.push_back(section);
						in_section = false;
					}
					else {
						std::cerr << "Line: " << line_counter << "; expected section name " << section.name << " but found " << found_name << "!\n";
						result.clear();
						break;
					}
				}
				else {
					std::string line_to_insert = trim(remove_line_comments(line));
					if (!line_to_insert.empty())
						section.lines.push_back({ line_to_insert, line_counter });
				}
			}
			else {				
				if (line.size() > 6) {	//".section_name begin" will have at least 6 chars					
					if ((line[0] == '.') && ends_with(line, begin_str)) {
						//looks, like a section begin
						section.name = trim(line.substr(1, line.size()- begin_str.size()-2));
						section.lines.clear();
						in_section = true;
					}
				}
			}
		}
	}
	else {
		std::cerr << "Cannot open file " << src_path.string() << std::endl;		
	}

	return result;
		
}

std::string slice_line_by(std::string& line, const std::string& delimiter, bool eol_delimits = false) {
	line = trim(line);
	const auto pos = line.find(delimiter);
	if (pos != std::string::npos) {
		std::string result = trim(line.substr(0, pos));
		line.erase(0, pos + delimiter.size());
		line = trim(line);
		return result;
	}
	else {
		if (eol_delimits) {
			std::string result = trim(line);
			line.clear();
			return result;
		} else			
			return std::string{};
	}
}


sirael_lang::TAttributes::TAttributes(std::string str) {
	mValid = true;

	while (!str.empty()) {
		slice_line_by(str, ".");
		const auto name = slice_line_by(str, "=");
		auto val_str = slice_line_by(str, " ");
		
		if (val_str.empty()) {
			val_str = slice_line_by(str, ")");
			str.clear();
		}

		bool ok = false;
		double val = str_2_dbl(val_str, ok);
		if (!ok) {
			bool tmp = str_2_bool(Widen_String(val_str), ok);
			if (ok)
				val = tmp ? 1.0 : 0.0;
		}
			

		if (ok) {
			mVals.insert({ name, val });
		}
		else {
			std::cerr << "Cannot parse value " << val_str << " for attribute " << name << "!\n";
			mValid = false;
		}
	}
}


bool sirael_lang::TAttributes::valid() const {
	return mValid;
}

double sirael_lang::TAttributes::val(const std::string& name, const double def_val) const {
	const auto iter = mVals.find(name);
	return iter != mVals.end() ? iter->second : def_val;
}

sirael_lang::TNames::TNames(const std::vector<std::string>& names) {
	for (const auto& name : names)
		push(name);
}

bool sirael_lang::TNames::push(const std::string &name) {
	const auto iter = mIds.find(name);
	const bool result = iter == mIds.end();
	if (result) {		
		mIds.insert({ name, mNext_Id });
		mNext_Id++;
	}

	return result;
}

sirael_lang::TSirael_Id sirael_lang::TNames::id(const std::string& name) const {
	if (name == dev_null_str)
		return sirael_lang::dev_null_id;

	const auto iter = mIds.find(name);
	return iter != mIds.end() ? iter->second : sirael_lang::Invalid_Id;
}

size_t sirael_lang::TNames::size() const {
	return mIds.size();
}

std::vector<std::string> sirael_lang::TNames::to_vector() const {
	//we do reverse mapping
	std::vector<std::string> result;
	
	for (const auto& val : mIds) {
		result.push_back(val.first);		
	}

	std::sort(result.begin(), result.end(), [this](const std::string& a, const std::string& b) {return id(a) < id(b); });
	return result;
}

sirael_lang::TSirael_Id Substance_Offset(const std::string& comp_name, const std::string& sub_name, const sirael_lang::TNames& comps, const sirael_lang::TNames& subs) {
	if ((comp_name == dev_null_str) || (sub_name == dev_null_str))
		return sirael_lang::dev_null_id;
	
	sirael_lang::TSirael_Id result = sirael_lang::Invalid_Id;

	const auto c_id = comps.id(comp_name);
	const auto s_id = subs.id(sub_name);
	if ((c_id != sirael_lang::Invalid_Id) && (s_id != sirael_lang::Invalid_Id)) {
		result = c_id * static_cast<sirael_lang::TSirael_Id>(subs.size()) + s_id;
	}
	else {
		std::cerr << "Cannot resolve " << comp_name << "/" << sub_name << "!\n";
	}

	return result;
}


std::optional<sirael_lang::TNames> Parse_Names(const sirael_lang::TRaw_Section& section) {
	sirael_lang::TNames result;

	for (const auto& line : section.lines) {
		if (line.line == dev_null_str) {
			std::cerr << dev_null_str << " is a reserved name! (" << line.line << ") in the section " << section.name << ", line no. " << line.line_no << std::endl;
			return std::nullopt;
		}

		if (!result.push(line.line)) {
			std::cerr << "Duplicate name (" << line.line << ") in the section " << section.name << ", line no. " << line.line_no << std::endl;
			return std::nullopt;
		}
	}

	return result;
}

size_t Get_Section_Id(const char* name, const std::vector<sirael_lang::TRaw_Section>& raw_sections, const size_t from_index = 0) {
	size_t result = std::numeric_limits<size_t>::max();

	for (size_t i = from_index; i < raw_sections.size(); i++)
		if (raw_sections[i].name == name) {
			result = i;
			break;
		}

	return result;
}



std::optional<std::vector<sirael_lang::TSignal>> Parse_Signals(const sirael_lang::TRaw_Section& raw_section, const sirael_lang::TNames& comps, const sirael_lang::TNames& subs) {
	const std::string emit_str = "emit";
	const std::string consume_str = "consume";

	const std::string conversion_factor_str = "conversion_factor";
	const std::string sensor_str = "sensor";

	std::vector<sirael_lang::TSignal> result;

	for (auto& line : raw_section.lines) {
		sirael_lang::TSignal signal;

		//find the flags aka action
		if (begins_with(line.line, emit_str)) {
			signal.flags = sirael_lang::NSignal_Flag::emit;
		} 
		else if (begins_with(line.line, consume_str)) {
			signal.flags = sirael_lang::NSignal_Flag::consume;
		}
		else {
			std::cerr << "Unkown instruction at line no. " << line.line_no << " (" << line.line << ")\n";
			return std::nullopt;
		}


		//we have the specific, now parse the common rest of the line
		std::string str = line.line;
		slice_line_by(str, ":");

		std::string sub_name, comp_name;		

		if (signal.flags && sirael_lang::NSignal_Flag::emit) {

			sub_name = trim(slice_line_by(str, " in "));
			comp_name = trim(slice_line_by(str, " as "));
			slice_line_by(str, "{");
			const std::string guid_str = "{" + slice_line_by(str, "}") + "}";
			bool ok = false;
			signal.signal_id = WString_To_GUID(Widen_String(guid_str), ok);
			if (!ok) {
				std::cerr << "Cannot convert GUID at line no. " << line.line_no << " (" << guid_str << ")\n";
				return std::nullopt;
			}
		}
		else {	//opcode is consume
			slice_line_by(str, "{");
			const std::string guid_str = "{" + slice_line_by(str, "}") + "}";
			slice_line_by(str, "as ");
			sub_name = trim(slice_line_by(str, " in "));
			comp_name = trim(slice_line_by(str, " ("));
			if (comp_name.empty()) {
				comp_name = str;
				str.clear();
			}

			bool ok = false;
			signal.signal_id = WString_To_GUID(Widen_String(guid_str), ok);
			if (!ok) {
				std::cerr << "Cannot convert GUID at line no. " << line.line_no << " (" << guid_str << ")\n";
				return std::nullopt;
			}


		}

		signal.substance_offset = Substance_Offset(comp_name, sub_name, comps, subs);
		if (signal.substance_offset == sirael_lang::Invalid_Id) {
			std::cerr << "Line no. " << line.line_no << " (" << line.line << ")\n";
			return std::nullopt;
		}

		sirael_lang::TAttributes attr{str};
		if (attr.valid()) {
			signal.conversion_ratio = attr.val(conversion_factor_str, 1.0);
			if (attr.val(sensor_str, 0.0) != 0.0)
				signal.flags |= sirael_lang::NSignal_Flag::sensor;
		}
		else {
			std::cerr << "Line no: " << line.line_no << std::endl;
			return std::nullopt;
		}

		result.push_back(signal);
	}

	return result;	
}


void Handle_Diffuse_Equalize_Line(sirael_lang::TInstruction& instr, std::string str, const sirael_lang::TNames& comps, const sirael_lang::TNames& subs) {
	const auto primary_sub_str = trim(slice_line_by(str, " in "));	//primary substance
	const auto src_comp = trim(slice_line_by(str, instr.opcode == sirael_lang::NInstruction::equalize ? "<->" : "->"));
	auto dst_comp = trim(slice_line_by(str, "(", true));

	const auto src_offset = Substance_Offset(src_comp, primary_sub_str, comps, subs);
	const auto dst_offset = Substance_Offset(dst_comp, primary_sub_str, comps, subs);

	if ((src_offset == sirael_lang::Invalid_Id) || (dst_offset == sirael_lang::Invalid_Id)) {
		std::cerr << "Cannot parse source and/or destination substance\n";
		instr.opcode = sirael_lang::NInstruction::invalid_encoding;
		return;
	}

	instr.src1 = src_offset;
	instr.src2 = dst_offset;

	sirael_lang::TAttributes attr{trim(str)};	
	if (!attr.valid()) {
		instr.opcode = sirael_lang::NInstruction::invalid_encoding;
		std::cerr << " Cannot parse the attributes!\n";
		return;
	}
		
	instr.parameters.transport.p = attr.val("p", 1.0);
	instr.parameters.transport.max_rate = attr.val("max_rate", 1.0);
}

void Handle_Reaction_Line(sirael_lang::TInstruction& instr, std::string str, const sirael_lang::TNames& comps, const sirael_lang::TNames& subs) {
	std::string primary_sub_str, secondary_sub_str;

	if ((instr.opcode == sirael_lang::NInstruction::react) || (instr.opcode == sirael_lang::NInstruction::inverse_react)) {
		primary_sub_str = trim(slice_line_by(str, " & "));	//primary substance  - [inverse] react

		if (primary_sub_str.empty())
			primary_sub_str = trim(slice_line_by(str, " -> "));
		else		
			secondary_sub_str = trim(slice_line_by(str, " -> "));			
		
		if (primary_sub_str.empty())
			instr.opcode = sirael_lang::NInstruction::invalid_encoding;
	}
	else if (instr.opcode == sirael_lang::NInstruction::inhibit) {
		primary_sub_str = trim(slice_line_by(str, " by "));	//primary substance  - inhibition
		if (!primary_sub_str.empty()) {
			secondary_sub_str = trim(slice_line_by(str, " -> "));
			if (secondary_sub_str.empty())
				instr.opcode = sirael_lang::NInstruction::invalid_encoding; //inhibition needs inhibiting substance
		} else
			instr.opcode = sirael_lang::NInstruction::invalid_encoding;			

		
	} else 
		instr.opcode = sirael_lang::NInstruction::invalid_encoding;

	const std::string product_str = trim(slice_line_by(str, " in "));
	if (product_str.empty())
		instr.opcode = sirael_lang::NInstruction::invalid_encoding;


	std::string comp_str = trim(slice_line_by(str, "("));
	if (comp_str.empty()) {
		comp_str = trim(str);
		str.clear();
	}


	if (instr.opcode == sirael_lang::NInstruction::invalid_encoding) {
		std::cerr << "Wrong encoding of a reaction!\n";
		return;
	}
	
	sirael_lang::TAttributes attr{trim(str)};
	if (!attr.valid()) {
		std::cerr << "Cannot parse attributes!\n";
		instr.opcode = sirael_lang::NInstruction::invalid_encoding;
	}
	


	const auto sub1_offset = Substance_Offset(comp_str, primary_sub_str, comps, subs);
	const auto sub2_offset = secondary_sub_str.empty() ? sirael_lang::dev_null_id :
			Substance_Offset(comp_str, secondary_sub_str, comps, subs);
	const auto prod_offset = Substance_Offset(comp_str, product_str, comps, subs);

	if ((sub1_offset == sirael_lang::Invalid_Id) || (prod_offset == sirael_lang::Invalid_Id) ||
		(sub2_offset == sirael_lang::Invalid_Id)) {
		std::cerr << "Cannot parse source and/or destination substance\n";
		instr.opcode = sirael_lang::NInstruction::invalid_encoding;
		return;
	}

	instr.src1 = sub1_offset;
	instr.src2 = sub2_offset;
	instr.product = prod_offset;

	instr.parameters.reaction.p = attr.val("p", 1.0);
	instr.parameters.reaction.max_rate = attr.val("max_rate", 1.0);
	instr.parameters.reaction.ratio = attr.val("ratio", 1.0);
	if (instr.opcode != sirael_lang::NInstruction::inverse_react)
		instr.parameters.reaction.product_fraction = attr.val("product_fraction", 1.0);
}

std::optional<std::vector<sirael_lang::TInstruction>> Parse_Metabolism(const sirael_lang::TRaw_Section& raw_section, const sirael_lang::TNames& comps, const sirael_lang::TNames& subs) {

	const std::string transport_str = "transport";
	const std::string passive_str = "passive";
	const std::string active_str = "active";
	const std::string threshold_str = "threshold";
	const std::string equalize_str = "equalize";

	const std::string react_str = "react";
	const std::string inverse_str = "inverse";
	const std::string inhibited_str = "inhibited";

	const std::string nop_str = "nop";

	std::vector<sirael_lang::TInstruction> result;

	for (size_t i = 0; i < raw_section.lines.size(); i++) {
		std::string str = raw_section.lines[i].line;

		sirael_lang::TInstruction instr;

		//resolve the instruction
		std::string instr_str = trim(slice_line_by(str, ":"));		
		const std::string modifier = trim(slice_line_by(instr_str, " "));		

		instr.opcode = sirael_lang::NInstruction::invalid_encoding;
		if (instr_str == transport_str) {
			if (modifier.empty() || (modifier == passive_str)) 
				instr.opcode = sirael_lang::NInstruction::passive_transport;	
			else if (modifier == active_str) 
				instr.opcode = sirael_lang::NInstruction::active_transport;
			else if (modifier == threshold_str)
				instr.opcode = sirael_lang::NInstruction::threshold_transport;


			if (instr.opcode != sirael_lang::NInstruction::invalid_encoding)
				Handle_Diffuse_Equalize_Line(instr, str, comps, subs);
			
		}
		else if (instr_str == equalize_str) {
			instr.opcode = sirael_lang::NInstruction::equalize;
			Handle_Diffuse_Equalize_Line(instr, str, comps, subs);
		}
		else if (instr_str == react_str) {
			if (modifier.empty())
				instr.opcode = sirael_lang::NInstruction::react;
			else if (modifier == inverse_str)
				instr.opcode = sirael_lang::NInstruction::inverse_react;
			else if (modifier == inhibited_str)
				instr.opcode = sirael_lang::NInstruction::inhibit;

			if (instr.opcode != sirael_lang::NInstruction::invalid_encoding)
				Handle_Reaction_Line(instr, str, comps, subs);
		}
		else if (instr_str == nop_str) {
			instr.opcode = sirael_lang::NInstruction::nop;
		}


		if (instr.opcode == sirael_lang::NInstruction::invalid_encoding) {
			std::cerr << "Unknown instruction encoding; line no. " << raw_section.lines[i].line_no << " (" << raw_section.lines[i].line << ")!\n";
			return std::nullopt;
		}


		
		
		result.push_back(instr);
	}

	return result;
}

std::optional<uint64_t> Parse_Global(const sirael_lang::TRaw_Section& raw_section) {
	const std::string stepping_str = "stepping";
	uint64_t result = sirael_lang::usecs_default_stepping;

	for (const auto& line : raw_section.lines) {
		if (begins_with(line.line, init_instr_str)) {
			std::string str = line.line.substr(init_instr_str.size());
			const auto val_str = trim(slice_line_by(str, "->"));
			bool ok = false;
			result = str_2_uint(val_str.c_str(), ok);
			if ((trim(str) != stepping_str) || !ok) {
				std::cerr << "Unknown global parameter; line no. " << line.line_no << " (" << line.line << ")!\n";
				return std::nullopt;
			}
			else
				result *= 1000 * 1000;	//success, convert number of seconds to the number of microseconds
		}
		else {
			std::cerr << "Invalid instruction in the global section; line no. " << line.line_no << " (" << line.line << ")!\n";
			return std::nullopt;
		}

	}

	return result;
}

std::optional<std::vector<double>> Parse_State(const sirael_lang::TRaw_Section& raw_section, const sirael_lang::TNames& comps, const sirael_lang::TNames& subs) {
	std::vector<double> result(comps.size() * subs.size(), 0.0);

	for (const auto& line : raw_section.lines) {
		if (begins_with(line.line, init_instr_str)) {
			std::string str = line.line.substr(init_instr_str.size());
			const auto val_str = trim(slice_line_by(str, " -> "));
			const auto sub_name = trim(slice_line_by(str, " in "));
			const auto comp_name = trim(str);

			bool ok = false;
			const double val = str_2_dbl(val_str, ok);
			const auto substance_offset = Substance_Offset(comp_name, sub_name, comps, subs);
			if (substance_offset < result.size())
				result[substance_offset] = val;
			else {
				std::cerr << "Invalid compartment/substance/value in the state section; line no. " << line.line_no << " (" << line.line << ")!\n";
				return std::nullopt;
			}
		}
		else {
			std::cerr << "Invalid instruction in the global section; line no. " << line.line_no << " (" << line.line << ")!\n";
			return std::nullopt;
		}

	}

	return result;
}


template <typename T>
bool is_any_max(const T val) { return val == std::numeric_limits<T>::max(); }

template <typename T, typename... A>
bool is_any_max(const T val, A... a) { return is_any_max(val) || is_any_max(a...); }

sirael_lang::TSirael_Program Raw_Sections_To_Program(const std::vector<sirael_lang::TRaw_Section>& raw_sections) {
	sirael_lang::TSirael_Program result;
	
	//proceed with mandatory sections
	const auto compartments_id = Get_Section_Id("compartments", raw_sections);
	const auto substances_id = Get_Section_Id("substances", raw_sections);
	const auto signals_id = Get_Section_Id("signals", raw_sections);
	const auto metabolism_id = Get_Section_Id("metabolism", raw_sections);
	
	if (is_any_max(compartments_id, substances_id, signals_id, metabolism_id)) {
		std::cerr << "The program needs compartments, substances, signals and metabolism sections declared!\n";
		return result;
	}
	
	const auto compartments_opt = Parse_Names(raw_sections[compartments_id]);
	const auto substances_opt = Parse_Names(raw_sections[substances_id]);
	

	if (!compartments_opt.has_value() || !substances_opt.has_value())
		return result;	//error already reported when parsing

	const auto compartments = compartments_opt.value();
	const auto substances = substances_opt.value();
	

	const auto signals_opt = Parse_Signals(raw_sections[signals_id], compartments, substances);
	const auto metabolism_opt = Parse_Metabolism(raw_sections[metabolism_id], compartments, substances);
	if ( !signals_opt.has_value() || !metabolism_opt.has_value())
		return result;	//error already reported when parsing

	
	result.declared_compartment_count = static_cast<sirael_lang::TSirael_Id>(compartments.size());
	result.declared_substances_count = static_cast<sirael_lang::TSirael_Id>(substances.size());
	
	result.signals = signals_opt.value();
	result.program = metabolism_opt.value();
	
	result.compartment_names = compartments.to_vector();
	result.substance_names = substances.to_vector();

	//from now on, proceed with the optional values
	const auto global_id = Get_Section_Id("global", raw_sections);
	const auto global_opt = Parse_Global(raw_sections[global_id]);
	if (global_opt.has_value())
		result.usecs_stepping = global_opt.value();
	


	const std::string state_str = "state";
	for (const auto& section : raw_sections) {
		if (section.name == state_str) {
			const auto state = Parse_State(section, compartments, substances);
			if (state.has_value())
				result.states.push_back(state.value());
			else {
				result = sirael_lang::TSirael_Program{};
				break;
			}
		}
	}

	return result;
}