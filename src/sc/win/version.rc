/**
 * Sirael Language Compiler
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */

#include <winver.h>


#define VER_COMPANYNAME_STR			"Tomas Koutny\0"
#define VER_LEGALCOPYRIGHT_STR		"Tomas Koutny\0"

#define VER_PRODUCTVERSION          2,0,0,0
#define VER_PRODUCTVERSION_STR      "2.0\0"
#define VER_PRODUCTNAME_STR		"Siraela\0"	

#ifndef DEBUG
#define VER_FILEFLAGS               VS_FF_PRERELEASE
#else
#define VER_FILEFLAGS              (VS_FF_PRIVATEBUILD|VS_FF_PRERELEASE|VS_FF_DEBUG)
#endif


//define VER_LANGNEUTRAL in files, which do not throw anything in English

#ifdef VER_LANGNEUTRAL
#define VER_VERSION_UNICODE_LANG  "000004B0" /* LANG_NEUTRAL/SUBLANG_NEUTRAL, Unicode CP */
#else
#define VER_VERSION_UNICODE_LANG  "040904B0" /* LANG_ENGLISH/SUBLANG_ENGLISH_US, Unicode CP */
#endif



#define VER_FILEDESCRIPTION_STR		"Sirael Language Compiler\0"
#define VER_INTERNALNAME_STR		"Siraela\0"

#define VER_FILEVERSION             2,0,0,0
#define VER_FILEVERSION_STR         "2.0.0.0\0"


#define	VER_ORIGINALFILENAME_STR	"sc.exe\0"

VS_VERSION_INFO VERSIONINFO
FILEVERSION    	VER_FILEVERSION
PRODUCTVERSION 	VER_PRODUCTVERSION
FILEFLAGSMASK  	VS_FFI_FILEFLAGSMASK
FILEFLAGS      	VER_FILEFLAGS
FILEOS         	VOS__WINDOWS32
FILETYPE       	VFT_DLL
FILESUBTYPE    	VFT2_UNKNOWN
BEGIN
    BLOCK "StringFileInfo"
    BEGIN
        BLOCK VER_VERSION_UNICODE_LANG
        BEGIN
            VALUE "FileDescription",  VER_FILEDESCRIPTION_STR
            VALUE "CompanyName",      VER_COMPANYNAME_STR
            VALUE "FileVersion",      VER_FILEVERSION_STR
            VALUE "InternalName",     VER_INTERNALNAME_STR
            VALUE "LegalCopyright",   VER_LEGALCOPYRIGHT_STR
//            VALUE "LegalTrademarks1", VER_LEGALTRADEMARKS1_STR
//            VALUE "LegalTrademarks2", VER_LEGALTRADEMARKS2_STR
            VALUE "OriginalFilename", VER_ORIGINALFILENAME_STR
            VALUE "ProductName",      VER_PRODUCTNAME_STR
            VALUE "ProductVersion",   VER_PRODUCTVERSION_STR
        END
    END

    BLOCK "VarFileInfo"
    BEGIN
        /* The following line should only be modified for localized versions.     */
        /* It consists of any number of WORD,WORD pairs, with each pair           */
        /* describing a language,codepage combination supported by the file.      */
        /*                                                                        */
        /* For example, a file might have values "0x409,1252" indicating that it  */
        /* supports English language (0x409) in the Windows ANSI codepage (1252). */

        VALUE "Translation", 0x409, 1250

    END
END