/**
 * Sirael Language Compiler
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */

#include "commands.h"

#include "decompiler.h"
#include "sirael_lang_parser.h"
#include <sirael/lang_parser/sirael_ir_writer.h>
#include <sirael/lang_parser/sirael_ir_reader.h>

#include <scgms//utils/string_utils.h>


#include <sstream>
#include <fstream>
#include <iostream>
#include <algorithm>

std::vector<std::string> Read_Lines(const filesystem::path& path) {
	std::vector<std::string> result;

	std::ifstream content(path);
	if (content.is_open()) {
		std::string line;
		while (std::getline(content, line))
			if (!line.empty())
				result.push_back(line);

		if (result.empty())
			std::wcerr << L"The list file is empty!\n";
	} else
		std::wcerr << L"Cannot open the list file!\n";

	return result;
}

int Call_Compile(const TOptions& options) {
	const auto pre_processed = Pre_Process_Source(options.input);
	if (pre_processed.empty())
		return __LINE__;

	const auto in_mem_program = Raw_Sections_To_Program(pre_processed);
	
	if (in_mem_program) {
		if (in_mem_program.states.empty())
			std::wcout << L"Source program contains no state." << std::endl;
		const auto banner = Read_Banner(options.banner, true);
		if (!banner.has_value())
			return __LINE__;

		std::string banner_val = banner.value();
		if (!Write_Program_IR(options.output, in_mem_program, banner_val.c_str(), options.save_debug_info))
			return __LINE__;
	}
	else
		return __LINE__;

	return 0;
}


int Call_Join(const TOptions& options) {
	const auto files_to_join = Read_Lines(options.input);
	if (files_to_join.empty())
		return __LINE__;	//msg already displayed

	std::stringstream original_banners;

	TLoaded_IR master = Read_Program_IR(files_to_join[0]);
	if (!master.valid) {
		std::cerr << master.error_msg << std::endl;
		return __LINE__;
	}

	//add the states
	original_banners << master.banner;
	std::string last_banner = master.banner;
	for (size_t i = 1; i < files_to_join.size(); i++) {
		TLoaded_IR next_ir = Read_Program_IR(files_to_join[i]);
		if (!next_ir.valid) {
			std::cerr << next_ir.error_msg << std::endl;
			return __LINE__;
		}
		if (last_banner != next_ir.banner) {	//avoid banner duplication
			original_banners << next_ir.banner;
			last_banner = next_ir.banner;
		}

		if (std::equal(master.program.program.begin(), master.program.program.end(), next_ir.program.program.begin(), next_ir.program.program.end(),
			[](const sirael_lang::TInstruction& i1, const sirael_lang::TInstruction& i2) {return memcmp(&i1, &i2, sizeof(sirael_lang::TInstruction)) == 0;})) {
			for (size_t state_idx = 0; state_idx < next_ir.program.states.size(); state_idx++)
				master.program.states.push_back(std::move(next_ir.program.states[state_idx]));
		}
		else {
			std::cerr << "Program " << files_to_join[i] << " has different instructions!" << std::endl;
			return __LINE__;
		}
	}

	//shall we replace the banner or put there the merged one?	
	if (options.banner.empty()) {
		master.banner = original_banners.str();
	}
	else {
		const auto new_banner = Read_Banner(options.banner, true);
		if (new_banner.has_value())
			master.banner = new_banner.value();
		else
			return __LINE__;
	}

	return Write_Program_IR(options.output, master.program, master.banner.c_str(), options.save_debug_info) ? 0 : __LINE__;
}


int Call_Split(const TOptions& options) {
	const auto state_id_pos = options.output.find(L'*');
	if (state_id_pos == std::wstring::npos) {
		std::cerr << "Output file mask does not contain * to indicate, where to put the state ordinal number!" << std::endl;
		return __LINE__;
	}

	if (options.output.find(L'*', state_id_pos + 1) != std::wstring::npos) {
		std::wcout << L"Output file mask cannot contain more than one * to indicate, where to put the state ordinal number!" << std::endl;
		return false;
	}


	const std::wstring output_prefix = options.output.substr(0, state_id_pos);
	const std::wstring output_postfix = options.output.substr(state_id_pos + 1);



	TLoaded_IR master = Read_Program_IR(options.input);
	if (!master.valid) {
		std::cerr << master.error_msg << std::endl;
		return __LINE__;
	}

	if (!options.banner.empty()) {
		const auto new_banner = Read_Banner(options.banner, true);
		if (new_banner.has_value())
			master.banner = new_banner.value();
		else
			return __LINE__;
	}


	sirael_lang::TSirael_Program single_state_program = master.program;
	for (size_t state_id = 0; state_id < master.program.states.size(); state_id++) {
		single_state_program.states = decltype(single_state_program.states){master.program.states[state_id]};
		if (!Write_Program_IR(output_prefix + std::to_wstring(state_id+1) + output_postfix, single_state_program, master.banner.c_str(), options.save_debug_info))
			return __LINE__;
	}

	return 0;
}


int Call_Decompile(const TOptions& options) {
	return Decompile(options.input, options.output, options.banner, options.save_debug_info);
}