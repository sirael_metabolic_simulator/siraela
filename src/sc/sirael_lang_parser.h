/**
 * Sirael Language Compiler
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */


#pragma once

#include <sirael/lang_parser/sirael_lang_adt.h>

#include <string>
#include <vector>

#include <scgms/rtl/FilesystemLib.h>


namespace sirael_lang {

	struct TLine {
		std::string line;
		size_t line_no;
	};


	struct TRaw_Section {
		std::string name;
		std::vector<TLine> lines;
	};


	class TNames {
	protected:
		std::map<std::string, TSirael_Id> mIds;
		TSirael_Id mNext_Id = 0;	//dev_null is the very first and always present id
	public:
		TNames() {};
		TNames(const std::vector<std::string>& names);
		bool push(const std::string& name);		//registers new name, returns true if inserted or false if already existed
		TSirael_Id id(const std::string& name) const;	//returns name id
		size_t size() const;


		std::vector<std::string> to_vector() const;
	};


	
	class TAttributes {
	protected:
		std::map<std::string, double> mVals;
		bool mValid = false;
	public:
		TAttributes(std::string str);
		double val(const std::string& name, const double def_val) const;
		bool valid() const;
	};

}

std::vector<sirael_lang::TRaw_Section> Pre_Process_Source(const filesystem::path src_path);
	//unlike map, vector will preserve the order in which the section were declared

sirael_lang::TSirael_Program Raw_Sections_To_Program(const std::vector<sirael_lang::TRaw_Section> &raw_sections);