/**
 * Sirael Language Compiler
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */

#include "decompiler.h"

#include <sirael/lang_parser/sirael_ir_reader.h>

#include <scgms/utils/string_utils.h>

#include <sstream>
#include <iostream>
#include <fstream>
#include <regex>
#include <chrono>
#include <iomanip>

#if (__cplusplus >= 202002L) || ((__cplusplus >= 199711L) && _MSC_VER)
	#define CPP20_supported
#else
	#pragma message("Please, consider upgrading to C++20")
#endif

#if !defined(CPP20_supported)
	tm* get_utc_pre_20() {

		auto now = std::chrono::system_clock::now();
		std::time_t current_time = std::chrono::system_clock::to_time_t(now);
		return gmtime(&current_time);				
	}
#endif

const char* rsNow_Var = "\\$\\(Now_UTC\\)";
//special chars R"([-[\]{}()*+?.,\^$|#\s]) must by escaped - e.g. with R"(\$&) via regex_replace


std::optional<std::string> Read_Banner(const std::filesystem::path& banner_file_path, bool eval_vars) {
	std::string result;
	if (!banner_file_path.empty()) {
		std::ifstream banner_file(banner_file_path);
		if (banner_file.is_open()) {
			std::stringstream buffer;
			buffer << banner_file.rdbuf();
			result = buffer.str();

			if (eval_vars) {
				std::stringstream utc_buff;
#if defined(CPP20_supported)
				utc_buff << std::chrono::utc_clock::now();
#else
				auto tm = get_utc_pre_20();
				utc_buff << std::put_time(tm, "%c %Z");
#endif

				result = std::regex_replace(result, std::regex(rsNow_Var), utc_buff.str());
			}
		}
		else {
			std::wcerr << L"Cannot open the banner file!\n";
			return std::nullopt;
		}
	}

	return result;
}

template <typename CN, typename SN>
std::string Dump_TInstruction_Lang(const sirael_lang::TInstruction& instr, const size_t substances_count, CN comp_name, SN sub_name) {

	std::stringstream result;

	auto decode_coord = [substances_count](const sirael_lang::TSirael_Id id)->sirael_lang::TSplit_Id {
		if (id == sirael_lang::dev_null_id)
			return { sirael_lang::dev_null_id, sirael_lang::dev_null_id };
		return sirael_lang::split_id(id, substances_count);		
	};

	auto encode_passive_transport = [&]() {
		const auto coord1 = decode_coord(instr.src1);
		const auto coord2 = decode_coord(instr.src2);

		result << "       transport: "
			<< sub_name(coord1.substance_id) << " in "
			<< comp_name(coord1.compartment_id) << " -> "
			<< comp_name(coord2.compartment_id);

		result << "\t(.p=" << instr.parameters.transport.p << " .max_rate=" << instr.parameters.transport.max_rate << ")";
		};

	auto encode_threshold_transport = [&]() {
		const auto coord1 = decode_coord(instr.src1);
		const auto coord2 = decode_coord(instr.src2);

		result << "threshold transport: "
			<< sub_name(coord1.substance_id) << " in "
			<< comp_name(coord1.compartment_id) << " -> "
			<< comp_name(coord2.compartment_id);

		result << "\t(.p=" << instr.parameters.transport.p << " .max_rate=" << instr.parameters.transport.max_rate << ")";
		};

	auto encode_equalize = [&]() {
		const auto coord1 = decode_coord(instr.src1);
		const auto coord2 = decode_coord(instr.src2);

		result << "        equalize: "
			<< sub_name(coord1.substance_id) << " in "
			<< comp_name(coord1.compartment_id) << " <-> "
			<< comp_name(coord2.compartment_id);

		result << "\t(.p=" << instr.parameters.transport.p << " .max_rate=" << instr.parameters.transport.max_rate << ")";
		};

	auto encode_active_transport = [&]() {
		const auto coord1 = decode_coord(instr.src1);
		const auto coord2 = decode_coord(instr.src2);

		result << "active transport: "
			<< sub_name(coord1.substance_id) << " in "
			<< comp_name(coord1.compartment_id) << " -> "
			<< comp_name(coord2.compartment_id);

		result << "\t(.p=" << instr.parameters.transport.p << " .max_rate=" << instr.parameters.transport.max_rate << ")";
		};


	auto encode_react = [&]() {
		const auto coord1 = decode_coord(instr.src1);
		const auto coord2 = decode_coord(instr.src2);
		const auto coordp = decode_coord(instr.product);

		result << "           react: "
			<< sub_name(coord1.substance_id);

		if (instr.src2 != sirael_lang::dev_null_id)
			result << " & " << sub_name(coord2.substance_id);

		result << " -> " << sub_name(coordp.substance_id)
			<< " in " << comp_name(coord1.compartment_id);	//the very first substances gives the compartment, where the reaction happens

		result << "\t(.p=" << instr.parameters.reaction.p << " .max_rate=" << instr.parameters.reaction.max_rate <<
			" .ratio=" << instr.parameters.reaction.ratio << " .product_fraction=" << instr.parameters.reaction.product_fraction << ")";
		};

	auto encode_inhibit = [&]() {
		const auto coord1 = decode_coord(instr.src1);
		const auto coord2 = decode_coord(instr.src2);
		const auto coordp = decode_coord(instr.product);

		result << " inhibited react: "
			<< sub_name(coord1.substance_id)
			<< " by " << sub_name(coord2.substance_id);

		result << " -> " << sub_name(coordp.substance_id)
			<< " in " << comp_name(coord1.compartment_id);

		result << "\t(.p=" << instr.parameters.inhibition.p << " .max_rate=" << instr.parameters.inhibition.max_rate <<
			" .ratio=" << instr.parameters.inhibition.ratio << " .product_fraction=" << instr.parameters.inhibition.product_fraction << ")";
		};

	auto encode_inverse_react = [&]() {
		const auto coord1 = decode_coord(instr.src1);
		const auto coord2 = decode_coord(instr.src2);
		const auto coordp = decode_coord(instr.product);

		result << "   inverse react: "
			<< sub_name(coord1.substance_id)
			<< " & " << sub_name(coord2.substance_id);

		result << " -> " << sub_name(coordp.substance_id)
			<< " in " << comp_name(coord1.compartment_id);

		result << "\t(.p=" << instr.parameters.inverse_reaction.p << " .max_rate=" << instr.parameters.inverse_reaction.max_rate << " .ratio=" << instr.parameters.inverse_reaction.ratio << ")";
		};


	switch (instr.opcode) {
		case sirael_lang::NInstruction::passive_transport:
			encode_passive_transport();
			break;

		case sirael_lang::NInstruction::equalize:
			encode_equalize();
			break;

		case sirael_lang::NInstruction::threshold_transport:
			encode_threshold_transport();
			break;			

		case sirael_lang::NInstruction::active_transport:
			encode_active_transport();
			break;

		case sirael_lang::NInstruction::react:
			encode_react();
			break;

		case sirael_lang::NInstruction::inhibit:
			encode_inhibit();
			break;

		case sirael_lang::NInstruction::inverse_react:
			encode_inverse_react();
			break;

		case sirael_lang::NInstruction::nop:
			result << "nop //no-operation instruction";
			break;

		case sirael_lang::NInstruction::end:
			result << "//end of program";
			break;

		default:
			result << "Unexpected instruction opcode! Opcode: " << static_cast<size_t>(instr.opcode);
			break;
	}


	return result.str();
}


int Decompile(const std::filesystem::path& ir_file, const std::filesystem::path& sirael_file, const std::filesystem::path& banner, const bool save_debug_info) {
	

	TLoaded_IR src = Read_Program_IR(ir_file);
	if (!src.valid) {
		std::cerr << src.error_msg << std::endl;
		return __LINE__;
	}

	std::ofstream dst{ sirael_file };
	if (dst.is_open()) {

		//write the banner
		if (!banner.empty()) {
			const auto new_banner = Read_Banner(banner, true);
			if (new_banner.has_value())
				dst << new_banner.value() << std::endl;
			else {
				std::wcerr << L"Cannot load the banner file!";
				return __LINE__;
			}
		}
		else {
			if (!src.banner.empty())
				dst << src.banner << std::endl;
		}

		dst << std::endl;

		auto gen_name = [save_debug_info](const size_t id, const std::vector<std::string>& names, const char* prefix)  -> std::string {
			if (id == sirael_lang::dev_null_id)
				return "dev_null";

			if (save_debug_info) 
				return id < names.size() ? names[id] : (prefix + std::to_string(id + 1 - names.size()));
			else
				return prefix + std::to_string(id+1);
		};

		auto sub_name = [&src, &gen_name](const size_t id)  -> std::string {
			return gen_name(id, src.program.substance_names, "unnamed_substance_");			
		};

		auto comp_name = [&src, &gen_name](const size_t id) -> std::string {
			return gen_name(id, src.program.compartment_names, "unnamed_compartment_");
		}; 

		//declare substances - real names, i.e., debug info is TODO
		dst << ".substances begin\n";
		for (size_t i = 0; i < src.program.declared_substances_count; i++) {
			dst << '\t' << sub_name(i) << std::endl;
		}
		dst << ".end substances\n\n";


		//declare compartments - real names, i.e., debug info is TODO
		dst << ".compartments begin\n";
		for (size_t i = 0; i < src.program.declared_compartment_count; i++) {
			dst << '\t' << comp_name(i) << std::endl;
		}
		dst << ".end compartments\n\n";



		dst << ".metabolism begin\n";
		for (size_t i = 0; i < src.program.program.size(); i++) {
			dst << '\t' << Dump_TInstruction_Lang(src.program.program[i], src.program.declared_substances_count, comp_name, sub_name) << std::endl;
		}
		dst << ".end metabolism\n\n";


		dst << ".signals begin\n";
		for (size_t i = 0; i < src.program.signals.size(); i++) {
			dst << '\t';
			const auto& sig = src.program.signals[i];

			const lldiv_t coord = lldiv(sig.substance_offset, src.program.declared_substances_count);

			if (sig.flags && sirael_lang::NSignal_Flag::emit) 
				dst << " emit: " << sub_name(coord.rem) << " in " << comp_name(coord.quot) << " as " << Narrow_WString(GUID_To_WString(sig.signal_id));
			else if (sig.flags && sirael_lang::NSignal_Flag::consume) 
				dst << " consume: " << Narrow_WString(GUID_To_WString(sig.signal_id)) << " as " << sub_name(coord.rem) << " in " << comp_name(coord.quot);			
			else {
				std::wcerr << L"Detected unknown signal handling for signal id: " << GUID_To_WString(sig.signal_id) << std::endl;
				return __LINE__;
			}


			const bool has_conversion_factor = sig.conversion_ratio != 1.0;
			const bool is_sensor = sig.flags && sirael_lang::NSignal_Flag::sensor;
			if (has_conversion_factor || is_sensor) {
				dst << " (";
				if (has_conversion_factor) {
					dst << ".conversion_factor=" << dbl_2_str(sig.conversion_ratio);
					if (is_sensor)
						dst << ' ';
				}

				if (is_sensor)
					dst << ".sensor=true";
				dst << ')';
			}

			dst << std::endl;
		}
		dst << ".end signals\n\n";


		for (size_t i = 0; i < src.program.states.size(); i++) {
			dst << ".state begin\n";

			const auto& state = src.program.states[i];

			for (size_t c = 0; c < src.program.declared_compartment_count; c++) {
				for (size_t s = 0; s < src.program.declared_substances_count; s++) {

					const double val = state[c * src.program.declared_substances_count + s];
					if (val != 0.0) {
						dst << "\t init: " << val;
						dst << " -> " << sub_name(s) << " in " << comp_name(c);
						dst << std::endl;
					}
				}
			}

			dst << ".end state\n\n";
		}
		

		dst << ".global begin\n";		
		dst << "\tinit: " << dbl_2_str(src.program.usecs_stepping / (1000.0 * 1000.0)) << " -> stepping //seconds\n";		
		dst << ".end global\n";

	} else {
		std::wcerr << L"Cannot open the output file!";
		return __LINE__;
	}


	return 0;
}