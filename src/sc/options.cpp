/**
 * Sirael Language Compiler
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 * 
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */

#include "options.h"

#include <scgms/rtl/UILib.h>
#include <scgms/rtl/FilesystemLib.h>

#include <misc/optionparser.h>


#include <iostream>

using TOption_Index = decltype(option::Descriptor::index);
enum class NOption_Index : TOption_Index {
        invalid = 0,    //for zero-terminated padding, must be zero
        unknown, 
        action,
        input_file,
        output_file,
        banner_file, 
        file_list_file,
        save_debug_info,
        license
        };


constexpr option::Descriptor Unknown_Option = { static_cast<TOption_Index>(NOption_Index::unknown), 0, "", "" ,option::Arg::None, "Usage: sc [options]\n\n"
											"Options:" };


constexpr option::Descriptor Input_File_Option = { static_cast<TOption_Index>(NOption_Index::input_file), 0, "i" , "input" ,option::Arg::Optional, "--input, -i=input \t\tpath to the input file" };
constexpr option::Descriptor Output_File_Option = { static_cast<TOption_Index>(NOption_Index::output_file), 0, "o" , "output" ,option::Arg::Optional, "--output, -o=output \t\tpath to the output file" };
constexpr option::Descriptor Header_File_Option = { static_cast<TOption_Index>(NOption_Index::banner_file), 0, "r" , "banner" ,option::Arg::Optional, "--banner, -r=banner \t\tpath to the banner file" };

constexpr option::Descriptor Compile_Action = { static_cast<TOption_Index>(NOption_Index::action), static_cast<TOption_Type>(NAction::compile), "c" , "compile" ,option::Arg::None, "--compile, -c\t\tcompiles source code to IR" };
constexpr option::Descriptor Decompile_Action = { static_cast<TOption_Index>(NOption_Index::action), static_cast<TOption_Type>(NAction::decompile), "d" , "decompile" ,option::Arg::None, "--decompile, -d\t\tdecompiles IR to source code" };
constexpr option::Descriptor Join_Action = { static_cast<TOption_Index>(NOption_Index::action), static_cast<TOption_Type>(NAction::join), "j" , "join" ,option::Arg::None, "--join, -j\t\tjoins multiple IR in order given by list in the -file" };
constexpr option::Descriptor Split_Action = { static_cast<TOption_Index>(NOption_Index::action), static_cast<TOption_Type>(NAction::split), "s" , "split" ,option::Arg::None, "--split, -s\t\tsplits IR to output, while replacing * with ordinary state number" };

constexpr option::Descriptor Save_Debug_Info_Option = { static_cast<TOption_Index>(NOption_Index::save_debug_info), 0, "" , "save_debug" ,option::Arg::None, "--save_debug\t\tsaves debug information" };

constexpr option::Descriptor Show_License_Option = { static_cast<TOption_Index>(NOption_Index::save_debug_info), 0, "" , "license" ,option::Arg::None, "--license\t\tshows license information" };


constexpr option::Descriptor Zero_Terminating_Option = { static_cast<TOption_Index>(NOption_Index::invalid), 0, nullptr , nullptr ,option::Arg::None, nullptr };

constexpr std::array<option::Descriptor, 11> option_syntax{ Unknown_Option, Input_File_Option, Output_File_Option, Header_File_Option,  
                                                            Compile_Action, Decompile_Action, Join_Action, Split_Action, Save_Debug_Info_Option,
                                                            Show_License_Option,
                                                            Zero_Terminating_Option };

void Show_Help() {
    option::printUsage(std::cout, option_syntax.data());    
}

void Print_Logo() {
    std::wcout << L"Sirael Language Compiler v1.0" << std::endl;
}


TOptions Parse_Options(const int argc, const char** argv) {    
    //declared the required structures for parsing the command line
    option::Stats  stats(option_syntax.data(), argc, argv);
    std::vector<option::Option> options(stats.options_max), buffer(stats.buffer_max);        
    option::Parser parse(option_syntax.data(), argc, argv, options.data(), buffer.data());
     
    //process the options
    TOptions result;
    result.action = NAction::invalid;


    //check cmd garbage
    const auto non_opt_cnt = parse.nonOptionsCount();
    if (non_opt_cnt > 0) {
        std::wcout << L"Detected some garbage on the command line, please, review the parameters and remove the non-conforming ones.\n";
        for (auto i = 0; i < non_opt_cnt; i++) {
            std::wcout << L'\t' << parse.nonOption(i) << std::endl;
        }
        std::wcout << std::endl;
        Show_Help();
        return result;
    }


    if (parse.error()) {
        std::wcout << L"Failed to parse the command line options!" << std::endl;
        Show_Help();
        return result;
    }

    
    const auto current_dir = std::filesystem::current_path();
    //and, get the input and output
    const auto& input_arg = options[static_cast<size_t>(NOption_Index::input_file)];
    if (input_arg) {
        result.input = Make_Absolute_Path(input_arg.last()->arg, current_dir);
    }   
    if (result.input.empty()) {
        std::wcout << L"Missing the input file argument!" << std::endl;
        Show_Help();
        return result;
    }



    const auto& output_arg = options[static_cast<size_t>(NOption_Index::output_file)];
    if (output_arg) {
        result.output = Make_Absolute_Path(output_arg.last()->arg, current_dir);
    }
    if (result.output.empty()) {
        std::wcout << L"Missing the output file argument!" << std::endl;
        Show_Help();
        return result;
    }


    // determine desired action
    auto& action_arg = options[static_cast<size_t>(NOption_Index::action)];

    if (action_arg) {
        //OK, the user set the action -> let's choose which one
        result.action = static_cast<NAction>(action_arg.last()->type());
        switch (result.action) {
            case NAction::compile:      [[fallthrough]];
            case NAction::decompile:    [[fallthrough]];
            case NAction::license:      [[fallthrough]];
            case NAction::join:         [[fallthrough]];
            case NAction::split:
                //nothing to do, supported action
                break;
            default:
                std::wcout << L"Detected not-implemented action!" << std::endl;
                result.action = NAction::invalid;
                return result;                
        }
    }


    const auto& banner_arg = options[static_cast<size_t>(NOption_Index::banner_file)];
    if (banner_arg) {
        result.banner = Make_Absolute_Path(banner_arg.last()->arg, current_dir);
    }
    
    const auto& save_debug_arg = options[static_cast<size_t>(NOption_Index::save_debug_info)];
    if (save_debug_arg) {
        result.save_debug_info = true;
    }


    return result;
}
