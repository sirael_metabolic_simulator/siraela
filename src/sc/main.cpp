/**
 * Sirael Language Compiler
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */

#include <iostream>


#include "options.h"
#include "commands.h"

#include <scgms/utils/winapi_mapping.h>

extern const char* raw_license;

int MainCalling main(int argc, const char** argv) {
	Print_Logo();
	int result = __LINE__;

	if (argc > 1) {

		TOptions what_to_do = Parse_Options(argc-1, argv+1);	//skip exe path


		switch (what_to_do.action) {
			case NAction::compile:
				result = Call_Compile(what_to_do);
				break;

			case NAction::decompile:
				result = Call_Decompile(what_to_do);
				break;

			case NAction::join:
				result = Call_Join(what_to_do);
				break;

			case NAction::split:
				result = Call_Split(what_to_do);
				break;

			case NAction::license:
				std::cout << raw_license << std::endl;
				result = 0;
				break;

			default:
				result = __LINE__;
				break;
		}
	}
	else {
		Show_Help();
		result = 0;
	}

	return result;

}

