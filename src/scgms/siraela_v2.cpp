/**
 * Siraela - in-silico patient model
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */

#include "siraela_v2.h"

#include <scgms/utils/string_utils.h>

CSiraela::CSiraela(scgms::IFilter* output) : CBase_Filter(output, siraela::siraela_v2_id) {
}

CSiraela::~CSiraela() {
	//
}



bool CSiraela::Execute_Fixed_Step(const double dt) {
	const double insulin_to_deliver = mIBR * dt;	//insulin delivered during the dt

	if (mIBR > 0.0)
		mVM.Try_Consume(scgms::signal_Requested_Insulin_Bolus, insulin_to_deliver);
	return mVM.Step();
}

bool CSiraela::Emit_Current_State() {
	auto emit_signal = [this](const GUID& signal_id, const double level)->bool {

		const auto classification = std::fpclassify(level);
		if ((classification == FP_NORMAL) || (classification == FP_ZERO)) {
			scgms::UDevice_Event evt{ scgms::NDevice_Event_Code::Level };

			evt.device_id() = siraela::siraela_v2_id;
			evt.device_time() = mCurrent_Time;
			evt.level() = level;
			evt.signal_id() = signal_id;
			evt.segment_id() = mCurrent_Segment_Id;

			return Succeeded(mOutput.Send(evt));
		}
		else
			return false;

	};

	mLast_State_Emit_Time = mCurrent_Time;
	return mVM.Emit_Signals(emit_signal);
}


HRESULT CSiraela::Do_Execute(scgms::UDevice_Event event) {
	if (!mSCGMS_Initialized && mVM_Initialized) {
		mCurrent_Segment_Id = event.segment_id();
		mCurrent_Time = event.device_time();
		mSCGMS_Initialized = true;
	}

	HRESULT res = S_FALSE;	//assume an event that's no input to us

	if (mSCGMS_Initialized && (event.event_code() == scgms::NDevice_Event_Code::Level)) {
		if (event.device_time() < mCurrent_Time)
			return E_ILLEGAL_STATE_CHANGE;	//got no time-machine to e.g., deliver insulin in the past


		if (event.segment_id() != mCurrent_Segment_Id) {
			Emit_Info(scgms::NDevice_Event_Code::Error, L"Siraela VM initialized- vs received- segment ids mismatch!", event.segment_id());
			return E_INVALIDARG;
		}


		//first of all, we have to catch up the time before the level's time
		//then we increase the amount by Try_Consume
		//hence, the next time we get level event, we process to  consume events

		res = Step_Until(event.device_time());
		if (Succeeded(res)) {
			const GUID sig_id = (event.signal_id() == scgms::signal_Carb_Rescue) ? scgms::signal_Carb_Intake : event.signal_id();
		
			if (sig_id == scgms::signal_Requested_Insulin_Basal_Rate) {
				mIBR = event.level() * 24.0;		//units per hour - i.e., dt will be a portion of a day, which equals 1.0 => *24.0
				event.signal_id() = scgms::signal_Delivered_Insulin_Basal_Rate;
				//As IBR-request is actually a pump-command, not a quantity for sure, it cannot be consumed.
				//By putting a sophisticated pump-simulator before the Siraela filter, you can obtain more realistic results.
				//Such a simulator would transform the IBR-requests to the series of insulin boluses
			}
			else {

				if (mVM.Try_Consume(sig_id, event.level())) {
					if (sig_id == scgms::signal_Requested_Insulin_Bolus)
						event.signal_id() = scgms::signal_Delivered_Insulin_Bolus;
				}
			}
			

			if (mCurrent_Time - mLast_State_Emit_Time > scgms::One_Minute)		//emit new state if vm has advanced
				//be aware that we have to advance the state or it may confuse the target log/display device
				//therefore, advancing period must be greater than the target log/display device resolution => at least one minute
				res = Emit_Current_State() ? S_OK : E_FAIL;
		}

	}
	else {
		if (event.event_code() == scgms::NDevice_Event_Code::Level) {
			if ((event.signal_id() == scgms::signal_Requested_Insulin_Basal_Rate) ||
				(event.signal_id() == scgms::signal_Requested_Insulin_Bolus) ||
				(event.signal_id() == scgms::signal_Carb_Intake) || (event.signal_id() == scgms::signal_Carb_Rescue) ||
				(event.signal_id() == scgms::signal_Heartbeat))
				res = E_ILLEGAL_STATE_CHANGE;	//cannot modify our state prior initialization!
		}
	}

	if (Succeeded(res))
		res = mOutput.Send(event);

	return res;

}

HRESULT CSiraela::Do_Configure(scgms::SFilter_Configuration configuration, refcnt::Swstr_list& error_description) {
	
	if (!configuration.Read_Bool(siraela::rsLicense_Agreed, false)) {
		error_description.push(L"You have to agree to the license terms! See the configuration options.");
		return E_INVALIDARG;
	}

		
	const filesystem::path ir_file = configuration.Read_File_Path(siraela::rsMetabolism_IR_File);
	const auto state_id = configuration.Read_Int(siraela::rsState_Index);

	const auto program = Read_Program_IR(ir_file);
	if (!program.valid) {
		error_description.push(Widen_String(program.error_msg));
		return E_INVALIDARG;
	}	

	if (static_cast<size_t>(state_id) >= program.program.states.size()) {
		error_description.push(L"Invalid state index");
		return E_INVALIDARG;
	}

	mVM_Initialized = mVM.Initialize(program.program, state_id);
	if (!mVM_Initialized) {
		error_description.push(L"Cannot initialize Siraela VM!");
		return E_UNEXPECTED;;
	}

	mFixed_Step = mVM.Stepping();

	return  program.banner.empty() ? S_OK : Emit_Info(scgms::NDevice_Event_Code::Information, Widen_String(program.banner));	
}


HRESULT CSiraela::Step_Until(const double future_time) {

	if (!mSCGMS_Initialized)
		return E_ILLEGAL_METHOD_CALL;

	bool executed_at_least_once = false;
	double next_time = mCurrent_Time + mFixed_Step;
	while (next_time <= future_time) {	//execute as long we do not exceed the stepped time
										//we do not allow going further, not even with a smaller stepping
										//as the parameters, as our approach is intended optimized for the fixed step

		if (!Execute_Fixed_Step(mFixed_Step))
			return E_FAIL;	//stop the simulation as the model got to a condition, which cannot be physiologically-plausibly recovered

		mCurrent_Time = next_time;
		next_time = mCurrent_Time + mFixed_Step;
		executed_at_least_once = true;
	}


	return executed_at_least_once ? S_OK : S_FALSE;
}
