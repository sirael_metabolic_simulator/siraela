/**
 * Siraela - in-silico patient model
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */

#include "descriptor.h"

#include "siraela_v2.h"

#include <scgms/iface/UIIface.h>
#include <scgms/rtl/hresult.h>
#include <scgms/rtl/ModelsLib.h>

#include <scgms/utils/descriptor_utils.h>


namespace siraela {

	const wchar_t* rsMetabolism_IR_File = L"Metabolism_IR_File";
	const wchar_t* rsState_Index = L"State_Index";
	const wchar_t* rsLicense_Agreed = L"License_Agreed";
	
	const size_t filter_param_count = 4;
	const scgms::NParameter_Type filter_param_types[filter_param_count] = { scgms::NParameter_Type::ptWChar_Array, scgms::NParameter_Type::ptInt64, scgms::NParameter_Type::ptBool};
	const wchar_t* filter_param_ui_names[filter_param_count] = { L"Metabolism IR file", L"State index", 
																 L"Agreed to the license terms of both Siraela and the metabolism file"};
	const wchar_t* filter_param_config_names[filter_param_count] = { rsMetabolism_IR_File,  rsState_Index, rsLicense_Agreed};
	const wchar_t* filter_param_tooltips[filter_param_count] = { nullptr, nullptr, nullptr};


	
	extern const GUID siraela_v2_id = { 0x378e028, 0x3694, 0x444b, { 0x87, 0xc2, 0x64, 0x1, 0xdc, 0x1f, 0xc7, 0x84 } }; // {0378E028-3694-444B-87C2-6401DC1FC784}


	const scgms::TFilter_Descriptor filter_desc = {
				siraela_v2_id,
				scgms::NFilter_Flags::None,
				L"Siraela v2",
				filter_param_count,
				filter_param_types,
				filter_param_ui_names,
				filter_param_config_names,
				filter_param_tooltips

	};

}




const std::array<scgms::TFilter_Descriptor, 1> filter_descriptions = { { siraela::filter_desc} };
	
	
DLL_EXPORT HRESULT IfaceCalling do_get_filter_descriptors(scgms::TFilter_Descriptor**begin, scgms::TFilter_Descriptor**end) {
	*begin = const_cast<scgms::TFilter_Descriptor*>(filter_descriptions.data());
	*end = *begin + filter_descriptions.size();
	return S_OK;
}


DLL_EXPORT HRESULT IfaceCalling do_create_filter(const GUID* id, scgms::IFilter* output, scgms::IFilter** filter) {
	if (!id || !filter)
		return E_INVALIDARG;

	if (*id == siraela::siraela_v2_id)
		return Manufacture_Object<CSiraela>(filter, output);

	return E_NOTIMPL;

}
