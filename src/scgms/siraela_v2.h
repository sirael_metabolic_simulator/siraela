/**
 * Siraela - in-silico patient model
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */

#pragma once

#include "descriptor.h"
#include <sirael/vm/siraela_vm.h>

#include <sirael/lang_parser/sirael_ir_reader.h>

#include <scgms/iface/DeviceIface.h>
#include <scgms/rtl/FilterLib.h>

#include <array>
#include <iostream>
#include <fstream>


#pragma warning( push )
#pragma warning( disable : 4250 ) // C4250 - 'class1' : inherits 'class2::member' via dominance

class CSiraela : public virtual scgms::CBase_Filter {
protected:
	double mFixed_Step = std::numeric_limits<double>::quiet_NaN();	//fixed-step period 
	bool Execute_Fixed_Step(const double dt);						//interprets the instructions, which execute the fixed step
	HRESULT Step_Until(const double future_time);
	bool Emit_Current_State();	//returns true if the current state looks OK
protected:
	sirael_lang::TSirael_Program mProgram;
	bool mSCGMS_Initialized = false;
	double mCurrent_Time = std::numeric_limits<double>::quiet_NaN();
	double mLast_State_Emit_Time = 0.0;	//0.0 to make sure that first state is emitted
	uint64_t mCurrent_Segment_Id = scgms::Invalid_Segment_Id;
	double mIBR = 0.0;	//current insulin basal rate
protected:
	bool mVM_Initialized = false;
	CSiraela_VM mVM;
protected:
	// scgms::CBase_Filter iface implementation
	virtual HRESULT Do_Execute(scgms::UDevice_Event event) override final;
	virtual HRESULT Do_Configure(scgms::SFilter_Configuration configuration, refcnt::Swstr_list& error_description) override final;
public:
	CSiraela(scgms::IFilter *output);
	virtual ~CSiraela();
};

#pragma warning( pop )
