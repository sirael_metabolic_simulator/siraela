const char* raw_logo = R"(
Sirael Console - in-silico patient model terminal
Copyright (c) since 2020 Tomas Koutny.

Contact:
tomas@sirael.science
Tomas Koutny


Purpose of this software:
This software is a personal, research project of Tomas Koutny. It is strictly
prohibited to use this software for diagnosis or treatment of any medical condition,
without proving a safety for the intended use.

By using this software, you are responsible for:
  a) implementing all safety requirements,
  b) proving the safety for a particular purpose,
  c) obtaining all regulatory approvals,
  d) any arising harm.

Especially, a diabetic patient is warned that improper use of this software
may result into severe injure, including death.

Licensing terms:
This software is granted under the Apache 2.0 license for a non-commercial use.
This software can be granted for a commercial use under a written agreement only.
Always, you must include and display this header-comment when redistributing the software in any form.

When referring to this work, use the following citation:
Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
Available at
  https://link.springer.com/article/10.1007/s44174-024-00199-9
  or https://doi.org/10.1007/s44174-024-00199-9
  or git: https://gitlab.com/sirael_metabolic_simulator/

This software uses some Apache2.0-licensed source files of the SmartCGMS project:
    * Tomas Koutny and Martin Ubl, "SmartCGMS as a Testbed for a Blood-Glucose Level Prediction and/or 
    * Control Challenge with (an FDA-Accepted) Diabetic Patient Simulation", Procedia Computer Science,  
    * Volume 177, pp. 354-362, 2020

This software uses the Lean Mean C++ Option Parser, Copyright (C) 2012-2017 Matthias S. Benkmann, with
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software, to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

)";