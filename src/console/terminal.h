/**
 * Sirael - in-silico patient model
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */


#include <sirael/vm/siraela_vm.h>
#include <sirael/vm/term_cmd.h>

#include <filesystem>
#include <iostream>

class CTerminal {
	
protected:
	std::unique_ptr<CSiraela_VM> mVM;
	virtual std::unique_ptr<CSiraela_VM> Create_New_VM();
	std::filesystem::path mRecent_SIR_Path;
protected:
	uint64_t mWall_Clock_Seconds = 0;
	uint64_t mJitter = 0;
	uint64_t mStepping = std::numeric_limits<decltype(mStepping)>::max();
	double mInsulin_Units_Per_Stepping = 0.0;
	bool Can_Call_Handle();	
	void Handle_Reset(std::filesystem::path ir, const size_t state_idx);
	bool Handle_Advance(size_t seconds);
	void Handle_Emit_State();
	protected:	
	bool mIBR_Warning_Printed = false;
	void Print_Help();	
public:
	void Try_Execute();
	sirael_term_cmd::NCommand Try_Execute_Line(const std::string &line);
	virtual ~CTerminal() {}
};