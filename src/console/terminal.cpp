/**
 * Sirael - in-silico patient model
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */


#include "terminal.h"

#include <sirael/lang_parser/sirael_ir_reader.h>

#include <scgms/iface/DeviceIface.h>
#include <scgms/utils/string_utils.h>

#include <sstream>
#include <map>
#include <vector>
#include <functional>

extern const char* raw_logo;


void CTerminal::Print_Help() {
	
	std::cout << "\n\nControl commands\n================\n";
	std::cout << "help - displays this help\n";
	std::cout << "reset state_idx IR_file_path - resets the simulator to particular (zero-indexed) state of a particular Sirael IR file\n";
	std::cout << "exit -  exits the simulator\n\n";


	std::cout << "Simulation commands\n===================\n";
	std::cout << "cho grams - makes the patient to consume a specified number of grams of carbohydrates\n";
	std::cout << "bolus units - makes the patient to receive a specified amount of insulin bolus\n";
	std::cout << "basal rate - makes the patient to receive insulin at a specified units/hour rate\n";
	std::cout << "activity ratio - sets current activity, 1.0 represents the basal metabolism\n";
	std::cout << "consume sig_guid quantity - consume a generic signal\n";
	std::cout << "advance seconds - advances the simulation up to the specified number of seconds\n";
	std::cout << "                  Be aware that SiraelVM steps at the loaded-program stepping granularity!\n";
	std::cout << "emit - prints the simulated quantities\n\n";
	
	std::cout << "\nPlease, note that if you will feed the simulator with intentionally implausible values,\nyou'll get intentionally wrong output.\n\n";

	std::cout << std::flush;
}

bool CTerminal::Can_Call_Handle() {
	bool result = mVM.operator bool();
	if (!result)
		std::cout << "Error: You must reset the Siraela VM first to load the particular metabolism and its state" << std::endl;
	return result;
}

void CTerminal::Try_Execute() {
	std::cout << raw_logo;
	std::cout << "Type help for a list of available commands." << std::endl;


	std::string line;
	while (std::getline(std::cin, line)) {		

		if (Try_Execute_Line(line) == sirael_term_cmd::NCommand::exit)
			break;
	}
}


sirael_term_cmd::NCommand CTerminal::Try_Execute_Line(const std::string &line) {
	const auto cmd = sirael_term_cmd::Parse(line);

	switch (cmd.command) {
			
		case sirael_term_cmd::NCommand::help: Print_Help();
			break;

		case sirael_term_cmd::NCommand::echo: std::cout << cmd.str_val << std::endl;
			break;

		case sirael_term_cmd::NCommand::reset: Handle_Reset(cmd.str_val, cmd.uint_val);
			break;

		case sirael_term_cmd::NCommand::cho:
			if (Can_Call_Handle()) {
				if (!mVM->Try_Consume(scgms::signal_Carb_Intake, cmd.real_val))
					std::cout << "Error: CHO was not eaten!" << std::endl;
				else
					std::cout << "Success: cho: " << cmd.real_val << "; WallClock: " << mWall_Clock_Seconds << "; CHO delivered" << std::endl;
			}					
			break;

		case sirael_term_cmd::NCommand::insulin_basal_rate:
			if (Can_Call_Handle()) {
				if (!mIBR_Warning_Printed) {
					std::cout << "Warning: Real pump does not deliver insulin continuously, but discretely as a sequence of small boluses.\n" <<
									"This terminal will provide least-effort simulation of a continuous delivery for initial experimentation.\n" <<
									"For a serious research, please, provide a real-pump simulator, which would deliver boluses only." << std::endl;
					mIBR_Warning_Printed = true;
				}

				mInsulin_Units_Per_Stepping = static_cast<double>(mStepping) * cmd.real_val / static_cast<double>(60 * 60);	//units per hour => convert to units per second					
				std::cout << "Success: basal: " << cmd.real_val << "; WallClock: " << mWall_Clock_Seconds << "; Insulin basal rate set." << std::endl;
			}
			break;

		case sirael_term_cmd::NCommand::insulin_bolus:
			if (Can_Call_Handle()) {
				if (!mVM->Try_Consume(scgms::signal_Requested_Insulin_Bolus, cmd.real_val))
					std::cout << "Error: Insulin bolus was not delivered!" << std::endl;
				else
					std::cout << "Success: bolus: " << cmd.real_val << "; WallClock: " << mWall_Clock_Seconds << "; bolus delivered" << std::endl;
			}
			break;

		case sirael_term_cmd::NCommand::heartbeat:
			if (Can_Call_Handle()) {
				if (!mVM->Try_Consume(scgms::signal_Heartbeat, cmd.real_val))
					std::cout << "Error: Heartbeat was not set!" << std::endl;
				else
					std::cout << "Success: heartbeat: " << cmd.real_val << "; WallClock: " << mWall_Clock_Seconds << "; heartbeat set" << std::endl;
			}
			break;

		case sirael_term_cmd::NCommand::activity:
			if (Can_Call_Handle()) {
				if (!mVM->Try_Consume(scgms::signal_Physical_Activity, cmd.real_val))
					std::cout << "Error: Activity was not set!" << std::endl;
				else				
					std::cout << "Success: activity: " << cmd.real_val << "; WallClock: " << mWall_Clock_Seconds << "; activity set" << std::endl;
			}
			break;

		case sirael_term_cmd::NCommand::consume_generic_signal:
			if (Can_Call_Handle()) {
				bool ok = false;
				if (ok) {
					if (!mVM->Try_Consume(cmd.sig_id, cmd.real_val))
						std::cout << "Error: Signal was not consumed!" << std::endl;
					else					
						std::cout << "Success: consume: " << cmd.real_val << "; WallClock: " << mWall_Clock_Seconds << "; signal consumed" << std::endl;
				}
				else
					std::cout << "Error: Invalid GUID!" << std::endl;

					
			}
			break;

		case sirael_term_cmd::NCommand::advance:
			if (Can_Call_Handle()) {
				if (!Handle_Advance(cmd.uint_val))
					return sirael_term_cmd::NCommand::failure;

			}
			break;

		case sirael_term_cmd::NCommand::emit_state:
			if (Can_Call_Handle())
				Handle_Emit_State();
			break;

		case sirael_term_cmd::NCommand::exit:
			return sirael_term_cmd::NCommand::exit;

		case sirael_term_cmd::NCommand::invalid: [[fallthrough]];
		default: //error was already reported
			break;
	}


	return cmd.command;
}


std::unique_ptr<CSiraela_VM> CTerminal::Create_New_VM() {
	return std::make_unique<CSiraela_VM>();
}

void CTerminal::Handle_Reset(std::filesystem::path ir, const size_t state_idx) {
	mVM.reset();
	mWall_Clock_Seconds = 0;
	mJitter = 0;
	mStepping = std::numeric_limits<decltype(mStepping)>::max();
	mInsulin_Units_Per_Stepping = 0.0;
	
	if (!ir.empty())
		mRecent_SIR_Path = ir;

	TLoaded_IR new_program = Read_Program_IR(mRecent_SIR_Path);
	if (new_program.valid) {

		if (state_idx <= new_program.program.states.size()) {
			std::unique_ptr<CSiraela_VM> new_vm = Create_New_VM();						

			if (new_vm->Initialize(new_program.program, state_idx)) {
				std::cout << new_program.banner;
				std::cout << "\nSuccess: Siraela VM is initialized and ready to use." << std::endl;				
				mVM = std::move(new_vm);
				mStepping = new_program.program.usecs_stepping / (1000 * 1000);	//convert from microseconds to seconds
			}
			else
				std::cout << "Error: Failed to initialize Siraela VM!" << std::endl;
		}
		else {
			std::cout << "Error: State index out of bounds for the loaded program!" << std::endl;
		}
	}
	else {
		std::cout << "Error: Cannot load the specified IR file!" << std::endl;
		std::cout << new_program.error_msg << std::endl;
	}
}

void CTerminal::Handle_Emit_State() {
	auto emit_signal = [this](const GUID& signal_id, const double level)->bool {

		const std::string sig_id_str = Narrow_WString(GUID_To_WString(signal_id));
		const std::string level_str = dbl_2_str(level);
	

		constexpr GUID signal_tissue_IG = { 0xb44f2278, 0xf592, 0x491e, { 0xbe, 0x20, 0x6d, 0xf6, 0x99, 0x9a, 0xc2, 0xd8 } };	// {B44F2278-F592-491E-BE20-6DF6999AC2D8}	
		constexpr GUID signal_blood_BG = { 0x1a03867f, 0x2f09, 0x4eb6, { 0x84, 0x99, 0xac, 0x1c, 0x21, 0x2, 0x5a, 0x2c } };		// {1A03867F-2F09-4EB6-8499-AC1C21025A2C
		std::string human_signal_name;
		if (signal_id == scgms::signal_Carb_Intake)
			human_signal_name = "CHO";
		else if (signal_id == signal_tissue_IG)
			human_signal_name = "IG (interstitial-fluid glucose level aka CGM sensor)";
		if (signal_id == signal_blood_BG)
			human_signal_name = "BG (blood glucose level)";

		std::cout << "Emit: "  << mWall_Clock_Seconds << "; " << sig_id_str << "; " << level_str << "; " << human_signal_name << "\n";
		return true;
	};

	std::cout << "Emitting state (wall clock in seconds; signal id; level; signal human name\n";
	mVM->Emit_Signals(emit_signal);	
	std::cout << "Success: State emitted" << std::endl;
}


bool CTerminal::Handle_Advance(size_t seconds) {		
	const auto start = mWall_Clock_Seconds;

	do {
		const auto desired_time = mWall_Clock_Seconds + seconds + mJitter;//note that Sirael advances by stepping, so it stops at or before the desired time

		while (mWall_Clock_Seconds + mStepping <= desired_time) {
			bool ok = mInsulin_Units_Per_Stepping == 0.0;

			if (!ok)
				ok = mVM->Try_Consume(scgms::signal_Requested_Insulin_Bolus, mInsulin_Units_Per_Stepping);

			if (!ok) {
				std::cout << "Error: Cannot deliver insulin at the scheduled rate! Simulator did not advance to the desired time!" << std::endl;
				return false;
			}

			if (mVM->Step())
				mWall_Clock_Seconds += mStepping;
			else {
				std::cout << "Error: Simulator did not advance to the desired time!" << std::endl;
				return false;
			}
		}

		mJitter = desired_time - mWall_Clock_Seconds;
		seconds = 0;	//we have already processed the requested seconds
	}  while (mJitter >= mStepping);	
	
	const auto stop = mWall_Clock_Seconds;	
	std::cout << "Success: advance: " << stop-start << "; WallClock: " << mWall_Clock_Seconds << "; clock advanced" << std::endl;
	return true;
}