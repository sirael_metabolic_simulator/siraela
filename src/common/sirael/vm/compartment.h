/**
 * Sirael - in-silico patient model
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */

#pragma once

#include <sirael/lang_parser/sirael_lang_adt.h>

struct TOffset_Descriptor {
	double conversion_factor = 1.0;
	bool sensor = false;
};

class IOffset_Descriptor {
public:
	virtual TOffset_Descriptor Describe(const sirael_lang::TSirael_Id compartment, const sirael_lang::TSirael_Id substance) = 0;
};

class CAbstract_Compartment {
public:	
	virtual sirael_lang::TSirael_Id id() = 0;

		//returns nan, if it contains no such substance
	virtual double level(const sirael_lang::TSirael_Id substance) = 0;				//returns current concentration/volume/value of a given substance

		//returns false if the operation does not make a (physiologically-plausible) sense
	virtual bool change_level(const sirael_lang::TSirael_Id substance, const double amount) = 0;
	virtual bool equalize(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) = 0;	//two-way diffusion - implemented to save the number of required parameters
	virtual bool passive_transport(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) = 0;
	virtual bool threshold_transport(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) = 0;
	virtual bool active_transport(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) = 0;
	virtual bool react(sirael_lang::TSirael_Id reactant, sirael_lang::TSirael_Id moderator, sirael_lang::TSirael_Id product, const sirael_lang::TParameters& params, const double dt) = 0;
	virtual bool inverse_react(sirael_lang::TSirael_Id detector, sirael_lang::TSirael_Id moderator, sirael_lang::TSirael_Id product, const sirael_lang::TParameters& params, const double dt) = 0;
	virtual bool inhibit(sirael_lang::TSirael_Id moderator, sirael_lang::TSirael_Id inhibitor, sirael_lang::TSirael_Id product, const sirael_lang::TParameters& params, const double dt) = 0;
};


class CNull_Compartment : public virtual CAbstract_Compartment {
public:	
	virtual ~CNull_Compartment() {};
	
	virtual sirael_lang::TSirael_Id id() final;

	virtual double level(const sirael_lang::TSirael_Id substance) final;
	virtual bool change_level(const sirael_lang::TSirael_Id substance, const double amount) final;
	virtual bool equalize(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) final;
	virtual bool react(sirael_lang::TSirael_Id reactant, sirael_lang::TSirael_Id moderator, sirael_lang::TSirael_Id product, const sirael_lang::TParameters& params, const double dt) final;
	virtual bool inverse_react(sirael_lang::TSirael_Id detector, sirael_lang::TSirael_Id moderator, sirael_lang::TSirael_Id product, const sirael_lang::TParameters& params, const double dt) final;
	virtual bool inhibit(sirael_lang::TSirael_Id moderator, sirael_lang::TSirael_Id inhibitor, sirael_lang::TSirael_Id product, const sirael_lang::TParameters& params, const double dt) final;

	virtual bool passive_transport(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) final;
	virtual bool active_transport(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) final;
	virtual bool threshold_transport(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) final;
};

class CGeneric_Compartment : public virtual CAbstract_Compartment {
protected:
	sirael_lang::TSirael_Id mId;
	std::vector<double> mSubstances;
	IOffset_Descriptor* mOffset_Descriptor = nullptr;
public:
	CGeneric_Compartment(const sirael_lang::TSirael_Id id, std::vector<double>::const_iterator initial_quantities_begin, std::vector<double>::const_iterator initial_quantities_end, IOffset_Descriptor* descriptor);
	virtual ~CGeneric_Compartment() {};

	virtual sirael_lang::TSirael_Id id() final;

	virtual double level(const sirael_lang::TSirael_Id substance) final;
	virtual bool change_level(const sirael_lang::TSirael_Id substance, const double amount) final;
	virtual bool equalize(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) final;
	virtual bool react(sirael_lang::TSirael_Id reactant, sirael_lang::TSirael_Id moderator, sirael_lang::TSirael_Id product, const sirael_lang::TParameters& params, const double dt) final;
	virtual bool inverse_react(sirael_lang::TSirael_Id detector, sirael_lang::TSirael_Id moderator, sirael_lang::TSirael_Id product, const sirael_lang::TParameters& params, const double dt) final;
	virtual bool inhibit(sirael_lang::TSirael_Id moderator, sirael_lang::TSirael_Id inhibitor, sirael_lang::TSirael_Id product, const sirael_lang::TParameters& params, const double dt) final;

	virtual bool passive_transport(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) final;
	virtual bool active_transport(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) final;
	virtual bool threshold_transport(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) final;
};
