/**
 * Sirael - in-silico patient model
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */
 //#define DPRINT_DEBUG

#pragma once

#include "compartment.h"

#include <vector>
#include <functional>
#include <fstream>

class CSiraela_VM : public virtual IOffset_Descriptor {
protected:
	double mStepping = std::numeric_limits<double>::quiet_NaN();
	std::vector<sirael_lang::TInstruction> mProgram;
	std::vector<std::unique_ptr<CGeneric_Compartment>> mCompartments;
	size_t mSubstances_Count = 0;
	std::unique_ptr<CNull_Compartment> mNull_Compartment;
	std::vector<TOffset_Descriptor> mDescriptors;	
	std::map<GUID, sirael_lang::TSirael_Id> mInput_Signals, mOutput_Signals;
protected:
	std::vector<std::string> mCompartment_Names, mSubstance_Names;
	bool mPrint_Debug =
#if defined(DPRINT_DEBUG)
		true;
#else
		false;
#endif // DEBUG

	std::ofstream mDebug_Stream;

	std::string Opcode_2_Str(const sirael_lang::NInstruction& opcode) {
		std::string result = "UNK";

		switch (opcode) {
			case sirael_lang::NInstruction::passive_transport:
				result = "PST";
				break;

			case sirael_lang::NInstruction::active_transport:
				result = "ACT";
				break;

			case sirael_lang::NInstruction::threshold_transport:
				result = "THT";
				break;

			case sirael_lang::NInstruction::equalize:
				result = "EQU";
				break;


			case sirael_lang::NInstruction::react:
				result = "RCT";
				break;

			case sirael_lang::NInstruction::inhibit:
				result = "INH";
				break;

			case sirael_lang::NInstruction::inverse_react:
				result = "INV";
				break;


			case sirael_lang::NInstruction::nop:
				result = "NOP";
				break;

			case sirael_lang::NInstruction::end:
				result = "END";
				break;

			case sirael_lang::NInstruction::invalid_encoding:
				result = "IVE";
				break;

		default:
			break;
		}
		
		return result;
	}


	std::string Get_Name(const size_t id, const std::vector<std::string>& names, const char* prefix);
	std::string Substance_Name(const size_t id);
	std::string Compartment_Name(const size_t id);	
protected:
	template <class T, bool (T::* F)(const sirael_lang::TSirael_Id, CAbstract_Compartment*, const sirael_lang::TParameters&, const double)>
	bool Handle_Transport(const sirael_lang::TInstruction& instruction) {
		const auto src_coord = lldiv(instruction.src1, mSubstances_Count);
		const auto dst_coord = lldiv(instruction.src2, mSubstances_Count);	//note that src2 may be devnull!
		
		const bool is_dst_null = instruction.src2 == sirael_lang::dev_null_id;

		T* src_comp = mCompartments[src_coord.quot].get();
		CAbstract_Compartment* dst_comp = is_dst_null ? static_cast<CAbstract_Compartment*>(mNull_Compartment.get()) : static_cast<CAbstract_Compartment*>(mCompartments[dst_coord.quot].get());
		
		double s_b = std::numeric_limits<double>::quiet_NaN(), s_a = std::numeric_limits<double>::quiet_NaN();
		double d_b = s_b, d_a = s_a;
		if (mPrint_Debug) {
			s_b = src_comp->level(static_cast<sirael_lang::TSirael_Id>(src_coord.rem));
			d_b = dst_comp->level(static_cast<sirael_lang::TSirael_Id>(dst_coord.rem));
		}

		const bool result = (src_comp->*F)(static_cast<sirael_lang::TSirael_Id>(src_coord.rem), dst_comp, instruction.parameters, mStepping);

		if (mPrint_Debug) {
			s_a = src_comp->level(static_cast<sirael_lang::TSirael_Id>(src_coord.rem));
			d_a = dst_comp->level(static_cast<sirael_lang::TSirael_Id>(dst_coord.rem));

			mDebug_Stream << Opcode_2_Str(instruction.opcode) << "-" << (result ? "S: " : "F: ") << Substance_Name(static_cast<sirael_lang::TSirael_Id>(src_coord.rem)) << ": ";
			mDebug_Stream << Compartment_Name(static_cast<sirael_lang::TSirael_Id>(src_coord.quot)) << "(" << s_b << " -> " << s_a << ", " << s_a - s_b << ") -> ";
			mDebug_Stream << Compartment_Name(is_dst_null ? sirael_lang::dev_null_id : static_cast<sirael_lang::TSirael_Id>(dst_coord.quot)) << "(" << d_b << " -> " << d_a << ", " << d_a - d_b << ")\n";
			mDebug_Stream << std::flush;
		}


		return result;
	};

	template <class T, bool (T::* F)(const sirael_lang::TSirael_Id, const sirael_lang::TSirael_Id, const sirael_lang::TSirael_Id, const sirael_lang::TParameters&, const double) >
	bool Handle_Reaction(const sirael_lang::TInstruction& instruction) {
		const auto src1_coord = lldiv(instruction.src1, mSubstances_Count);
		const sirael_lang::TSirael_Id src2 = instruction.src2 == sirael_lang::dev_null_id ? sirael_lang::dev_null_id : (instruction.src2 % mSubstances_Count) ;
		const sirael_lang::TSirael_Id product = instruction.product == sirael_lang::dev_null_id ? sirael_lang::dev_null_id : (instruction.product % mSubstances_Count);
		

		T* comp = mCompartments[src1_coord.quot].get();

		double s1_b = std::numeric_limits<double>::quiet_NaN();
		double s1_a = s1_b, s2_b = s1_b, s2_a = s1_b, p_b = s1_b, p_a = s1_b;
		if (mPrint_Debug) {
			s1_b = comp->level(static_cast<sirael_lang::TSirael_Id>(src1_coord.rem));
			s2_b = comp->level(static_cast<sirael_lang::TSirael_Id>(src2));
			p_b = comp->level(static_cast<sirael_lang::TSirael_Id>(product));
		}


		const bool result = (comp->*F)(static_cast<sirael_lang::TSirael_Id>(src1_coord.rem), src2, product, instruction.parameters, mStepping);


		if (mPrint_Debug) {
			s1_a = comp->level(static_cast<sirael_lang::TSirael_Id>(src1_coord.rem));
			s2_a = comp->level(static_cast<sirael_lang::TSirael_Id>(src2));
			p_a = comp->level(static_cast<sirael_lang::TSirael_Id>(product));

			auto print_substance = [&](const sirael_lang::TSirael_Id s_id, const double b, const double a) {
				mDebug_Stream << Substance_Name(s_id) << "(" << b << " -> " << a << "; " << a - b << ")";
			};

			mDebug_Stream << Opcode_2_Str(instruction.opcode) << "-" << (result ? "S: " : "F: ") << Compartment_Name(static_cast<sirael_lang::TSirael_Id>(src1_coord.quot)) << ": ";
			print_substance(static_cast<sirael_lang::TSirael_Id>(src1_coord.rem), s1_b, s1_a); mDebug_Stream << "; ";
			print_substance(static_cast<sirael_lang::TSirael_Id>(src2), s2_b, s2_a); mDebug_Stream << "; ";
			print_substance(static_cast<sirael_lang::TSirael_Id>(product), p_b, p_a); mDebug_Stream << std::endl;
			mDebug_Stream << std::flush;
		}


		return result;
	};	
public:
	CSiraela_VM();
	virtual ~CSiraela_VM();

	virtual bool Initialize(const sirael_lang::TSirael_Program& program, const size_t state_index);
	virtual bool Step();
	virtual bool Try_Consume(const GUID& id, double quantity);	//true if consumed, false on error
	double Stepping() const;
	bool Emit_Signals(std::function<bool(const GUID &id, const double value)> func);	//returns false if at least one signal failed

	virtual TOffset_Descriptor Describe(const sirael_lang::TSirael_Id compartment, const sirael_lang::TSirael_Id substance) override final;
};