/**
 * Sirael - in-silico patient model
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */


#include "term_cmd.h"

#include <scgms/utils/string_utils.h>

#include <sstream>

namespace sirael_term_cmd {

	TCommand Parse_NoArg(const std::vector<std::string>& args, const NCommand command) {
		TCommand result = { command, "", 0, 0.0 };
		for (const auto& arg : args) {
			result.str_val.append(" ");
			result.str_val.append(arg);
		}

		return result;
	}

	TCommand Parse_Single_String(const std::vector<std::string>& args, const NCommand command) {
		TCommand result = { command, "", 0, 0.0 };

		for (size_t i = 1; i < args.size(); i++) {
			if (i > 1)
				result.str_val += ' ';
			result.str_val += args[i];
		}

		return result;
	}

	TCommand Parse_Single_Integer(const std::vector<std::string>& args, const NCommand command) {
		TCommand result = { NCommand::invalid, "", 0, 0.0 };

		if (args.size() == 2) {
			bool ok = false;
			result.uint_val = str_2_uint(args[1].c_str(), ok);

			if (!ok) {
				result.command = NCommand::invalid;
				std::cout << "Invalid arguments for this command.";
			}
			else
				result.command = command;
		}
		else
			std::cout << "Error: Invalid number of parameters." << std::endl;

		return result;
	}

	TCommand Parse_Single_Real(const std::vector<std::string>& args, const NCommand command) {
		TCommand result = { NCommand::invalid, "", 0, 0.0 };

		if (args.size() == 2) {
			bool ok = false;
			result.real_val = str_2_dbl(args[1].c_str(), ok);

			if (!ok || (result.real_val < 0.0)) {
				result.command = NCommand::invalid;
				std::cout << "Invalid arguments for this command.";
				if (ok)
					std::cout << " It must be positive number.";
				std::cout << std::endl;
			}
			else
				result.command = command;
		}
		else
			std::cout << "Error: Invalid number of parameters." << std::endl;

		return result;
	}


	TCommand Parse_Reset(const std::vector<std::string>& args, const NCommand) {
		TCommand result = { NCommand::invalid, "", 0, 0.0 };

		
		if (args.size() == 2) {
			//short-version reset, keeps currrent .sir, just resets the state
			bool ok = false;
			result.uint_val = str_2_uint(args[1].c_str(), ok);
			if (ok)
				result.command = NCommand::reset;
		}

		else if (args.size() >= 3) {
			bool ok = false;
			result.uint_val = str_2_uint(args[1].c_str(), ok);

			std::string tmp = args[2];	//concat all the strings after the state to the handle possible white spaces in the file path
			for (size_t i = 3; i < args.size(); i++) {
				tmp += ' ';
				tmp += args[i];
			}
			result.str_val = tmp;

			if (!ok || result.str_val.empty()) {
				result.command = NCommand::invalid;
				std::cout << "Error: Invalid arguments for the reset command." << std::endl;
			}
			else
				result.command = NCommand::reset;
		}
		else
			std::cout << "Error: Invalid number of parameters." << std::endl;

		return result;
	}

	TCommand Parse_Generic_Consume(const std::vector<std::string>& args, const NCommand) {
		TCommand result = { NCommand::invalid, "", 0, 0.0 };

		if (args.size() == 3) {

			bool ok = false;
			result.str_val.clear();
			result.sig_id = WString_To_GUID(Widen_String(args[1]), ok);
			
			if (ok)
				result.real_val = str_2_dbl(args[2].c_str(), ok);


			if (!ok || (result.real_val < 0.0)) {
				result.command = NCommand::invalid;
				std::cout << "Error: Invalid arguments for the consume command." << std::endl;
			}
			else
				result.command = NCommand::consume_generic_signal;
		}
		else
			std::cout << "Error: Invalid number of parameters." << std::endl;

		return result;
	}

	using TParse_Sub_Func = std::function<TCommand(const std::vector<std::string>&, const NCommand)>;

	struct  TParse_Rec {
		NCommand cmd;
		TParse_Sub_Func func;
	};

	const std::map<std::string, TParse_Rec> commands = {
		{"help", {NCommand::help, Parse_NoArg}},
		{"echo", {NCommand::echo, Parse_Single_String}},
		{"reset", {NCommand::reset, Parse_Reset}},
		{"exit", {NCommand::exit, Parse_NoArg}},

		{"cho", {NCommand::cho, Parse_Single_Real} },
		{"bolus", {NCommand::insulin_bolus, Parse_Single_Real}},
		{"basal", {NCommand::insulin_basal_rate, Parse_Single_Real}},
		{"heartbeat", {NCommand::heartbeat, Parse_Single_Real}},
		{"activity", {NCommand::activity, Parse_Single_Real}},
		{"consume", {NCommand::consume_generic_signal, Parse_Generic_Consume}},
		{"advance", {NCommand::advance, Parse_Single_Integer}},
		{"emit", {NCommand::emit_state, Parse_NoArg}},
	};


	TCommand Parse(const std::string& line) {

		TCommand result = { sirael_term_cmd::NCommand::invalid, "", 0 };

		std::vector<std::string> args;


		{
			std::stringstream ss(line);
			while (!ss.eof()) {
				std::string single_arg;
				ss >> single_arg;
				if (!single_arg.empty())
					args.push_back(std::move(single_arg));
			}
		}

		if (!args.empty()) {

			const auto iter = commands.find(args[0]);
			if (iter != commands.end()) {
				result.command = iter->second.cmd;

				if (result.command < NCommand::sentinel) {
					result = iter->second.func(args, result.command);

				}
				else {
					result.command = NCommand::invalid;
					std::cout << "Error: Invalid command!" << std::endl;
				}
			}
		}


		return result;
	}

}