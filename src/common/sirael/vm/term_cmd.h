/**
 * Sirael - in-silico patient model
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */


#include <sirael/vm/siraela_vm.h>

#include <scgms/rtl/guid.h>

#include <filesystem>
#include <iostream>

namespace sirael_term_cmd {

	enum class NCommand {
		help,
		echo,
		reset,
		exit,

		cho,
		insulin_bolus,
		insulin_basal_rate,
		activity,
		heartbeat,
		consume_generic_signal,
		advance,
		emit_state,

		sentinel,
		invalid,
		failure

	};

	struct TCommand {
		NCommand command;
		std::string str_val;
		uint64_t uint_val;
		double real_val;
		GUID sig_id;
	};


	TCommand Parse(const std::string& line);
}