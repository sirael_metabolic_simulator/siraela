/**
 * Sirael - in-silico patient model
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */

#include "siraela_vm.h"

#include <filesystem>

CSiraela_VM::CSiraela_VM() {
	if (!mPrint_Debug) {
#ifdef _MSC_VER 
		std::array<char, 10> buf;
		char* sys = buf.data();
		size_t s = buf.size();
		if (_dupenv_s(&sys, &s, "SIRAEL_PRINT_DEBUG") != 0)
			sys = nullptr;
#else
		const char* sys = std::getenv("SIRAEL_PRINT_DEBUG");
#endif
		if (sys)
			mPrint_Debug = (sys[0] == '1') || (sys[0] == 'T') || (sys[0] == 't');
	}
}

CSiraela_VM::~CSiraela_VM() {
	if (mPrint_Debug) {
		mDebug_Stream << std::flush;		
	}
}


std::string CSiraela_VM::Get_Name(const size_t id, const std::vector<std::string>& names, const char* prefix){
	if (id == sirael_lang::dev_null_id)
		return "dev_null";

	return id < names.size() ? names[id] : (prefix + std::to_string(id + 1 - names.size()));
}

std::string CSiraela_VM::Substance_Name(const size_t id) {
	return Get_Name(id, mSubstance_Names, "unnamed_substance_");
}

std::string CSiraela_VM::Compartment_Name(const size_t id) {
	return Get_Name(id, mCompartment_Names, "unnamed_compartment_");
}


bool CSiraela_VM::Initialize(const sirael_lang::TSirael_Program& program, const size_t state_index) {
	mStepping = scgms::One_Second * static_cast<double>(program.usecs_stepping) / (1000.0 * 1000.0);
	mSubstances_Count = static_cast<size_t>(program.declared_substances_count);


	mCompartment_Names = program.compartment_names;
	mSubstance_Names = program.substance_names;		

	mProgram.assign(program.program.cbegin(), program.program.cend());
	

	mDescriptors.resize(static_cast<size_t>(program.declared_compartment_count) * mSubstances_Count);
	std::fill(mDescriptors.begin(), mDescriptors.end(), TOffset_Descriptor{});
	for (const auto& signal : program.signals) {
		if (signal.flags && sirael_lang::NSignal_Flag::emit)
			mOutput_Signals.insert({ signal.signal_id, signal.substance_offset } );

		if (signal.flags && sirael_lang::NSignal_Flag::consume)
			mInput_Signals.insert({ signal.signal_id, signal.substance_offset });

		mDescriptors[signal.substance_offset] = TOffset_Descriptor{signal.conversion_ratio, signal.flags && sirael_lang::NSignal_Flag::sensor };
	}

	mCompartments.clear();
	for (size_t i = 0; i < program.declared_compartment_count; i++) {		
		/*std::vector<double>::const_iterator*/ auto bg = program.states[state_index].begin() + i * mSubstances_Count;
		auto ed = bg + mSubstances_Count;
		mCompartments.push_back(std::make_unique<CGeneric_Compartment>(static_cast<sirael_lang::TSirael_Id>(i), bg, ed, static_cast<IOffset_Descriptor*>(this)));
	}

	mNull_Compartment = std::make_unique<CNull_Compartment>();


	if (mPrint_Debug) {
		std::string fname = "siraela";
//		fname.append(std::to_string(segment_id));
		fname.append(".trace");

		std::filesystem::path dst = std::filesystem::current_path() / fname;
		mDebug_Stream.open(dst, std::ios_base::out | std::ios_base::trunc);
	}

	return true;
}

TOffset_Descriptor CSiraela_VM::Describe(const sirael_lang::TSirael_Id compartment, const sirael_lang::TSirael_Id substance) {
	return mDescriptors[compartment * mSubstances_Count + substance];
}

bool CSiraela_VM::Try_Consume(const GUID& id, double quantity) {
	bool result = false;
	
	const auto iter = mInput_Signals.find(id);
	if (iter != mInput_Signals.end()) {
		const auto offset = sirael_lang::split_id(iter->second, mSubstances_Count);
		if (static_cast<size_t>(offset.compartment_id) < mCompartments.size()) {
			result = mCompartments[offset.compartment_id]->change_level(static_cast<sirael_lang::TSirael_Id>(offset.substance_id), quantity * mDescriptors[iter->second].conversion_factor);

			if (mPrint_Debug) {
				if (quantity > 0.0)
					mDebug_Stream << "ADD-S: " << quantity << "; substance: " << Substance_Name(static_cast<sirael_lang::TSirael_Id>(offset.substance_id)) << std::endl;
			}
		}
	}

	return result;
}

bool CSiraela_VM::Step() {

	bool result = true;

	for (size_t i = 0; result && (i < mProgram.size()); i++) {
		const sirael_lang::TInstruction& instruction = mProgram[i];

		switch (instruction.opcode) {
			//Handle_* functions could operate on CAbstract_Compartment, but only generic and null compartments
			//inherit from it. As null compartment cannot initiate any action, let's take a shortcut and use generic compartment directly.

			case sirael_lang::NInstruction::passive_transport:
				result &= Handle_Transport<CGeneric_Compartment, &CGeneric_Compartment::passive_transport>(instruction);
				//result &= Handle_Transport<CAbstract_Compartment, &CAbstract_Compartment::passive_transport>(instruction); -- see the comment above
				assert(result);
				break;

			case sirael_lang::NInstruction::equalize:
				result &= Handle_Transport<CGeneric_Compartment, &CGeneric_Compartment::equalize>(instruction);
				assert(result);
				break;

			case sirael_lang::NInstruction::active_transport:
				result &= Handle_Transport<CGeneric_Compartment, &CGeneric_Compartment::active_transport>(instruction);
				assert(result);
				break;

			case sirael_lang::NInstruction::threshold_transport:
				result &= Handle_Transport<CGeneric_Compartment, &CGeneric_Compartment::threshold_transport>(instruction);
				assert(result);
				break;

			case sirael_lang::NInstruction::react:
				result &= Handle_Reaction<CGeneric_Compartment, &CGeneric_Compartment::react>(instruction);
				assert(result);
				break;

			case sirael_lang::NInstruction::inhibit:
				result &= Handle_Reaction<CGeneric_Compartment, &CGeneric_Compartment::inhibit>(instruction);
				assert(result);
				break;

			case sirael_lang::NInstruction::inverse_react:
				result &= Handle_Reaction<CGeneric_Compartment, &CGeneric_Compartment::inverse_react>(instruction);
				assert(result);
				break;


			case sirael_lang::NInstruction::nop:
				break;

			case sirael_lang::NInstruction::end:
				return result;


			default:
				return false; //invalid opcode
			}

	}

	if (mPrint_Debug)
		mDebug_Stream << std::endl;


	return result;
}

double CSiraela_VM::Stepping() const {
	return mStepping;
}

bool CSiraela_VM::Emit_Signals(std::function<bool(const GUID& id, const double value)> func) {
	bool result = true;

	for (const auto& sig : mOutput_Signals) {
		const auto& desc = mDescriptors[sig.second];

		const auto coord = sirael_lang::split_id(sig.second, mSubstances_Count);
		const double level = mCompartments[static_cast<size_t>(coord.compartment_id)]->level(coord.substance_id);

		result &= func(sig.first, desc.conversion_factor * level);
			//do not break on false, try to emit as much info as possible
	}

	return result;
}