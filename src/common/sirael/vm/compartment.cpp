/**
 * Sirael - in-silico patient model
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */

#include "compartment.h"

#include <cmath>

#undef min

bool Is_Positive(const double number) {
	const auto fp = std::fpclassify(number);
	return (fp == FP_ZERO) || ((fp == FP_NORMAL) && (number > 0.0));
}

double Subnormal_to_Zero(const double number) {
	return std::fpclassify(number) == FP_SUBNORMAL ? 0.0 : number;
}

double Reacting_Potential(const double mass, const double factor) {	
	assert(factor >= 0.0);
	return mass > 0.0 ? (mass / (factor + mass)) : 0.0;
}

double Reacting_Product_By_Potential(const double reactant_mass, const double reacting_potential, const double max_rate, const double dt) {
	const double max_mass = dt * max_rate * reacting_potential;
	const double reacting_mass = std::min(max_mass, reactant_mass);

	return reacting_mass;
}


double Reacting_Product(const double reactant_mass, const double p, const double max_rate, const double dt) {
	const double reacting_potential = Reacting_Potential(reactant_mass, p);
	return Reacting_Product_By_Potential(reactant_mass, reacting_potential, max_rate, dt);
}
	

sirael_lang::TSirael_Id CNull_Compartment::id() {
	return sirael_lang::dev_null_id;
}

double CNull_Compartment::level(const sirael_lang::TSirael_Id substance) {
	return 0.0;	
}

bool CNull_Compartment::change_level(const sirael_lang::TSirael_Id substance, const double amount) {
	return true;	//we absorb anything and throw it to the void immediately
}

bool CNull_Compartment::equalize(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) {
	return false;	//cannot equalize with void
}

bool CNull_Compartment::passive_transport(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) {
	return false;	//cannot move anything out of the void
}

bool CNull_Compartment::active_transport(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) {
	return false;	//cannot move anything out of the void
}


bool CNull_Compartment::threshold_transport(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) {
	return false;	//cannot move anything out of the void
}

bool CNull_Compartment::react(sirael_lang::TSirael_Id reactant, sirael_lang::TSirael_Id moderator, sirael_lang::TSirael_Id product, const sirael_lang::TParameters& params, const double dt) {
	return false;	//in the void, there's nothing that could react
}

bool CNull_Compartment::inverse_react(sirael_lang::TSirael_Id detector, sirael_lang::TSirael_Id moderator, sirael_lang::TSirael_Id product, const sirael_lang::TParameters& params, const double dt) {
	return false;	//in the void, there's nothing that could react
}

bool CNull_Compartment::inhibit(sirael_lang::TSirael_Id moderator, sirael_lang::TSirael_Id inhibitor, sirael_lang::TSirael_Id product, const sirael_lang::TParameters& params, const double dt) {
	return false;	//in the void, there's nothing that could react
}


CGeneric_Compartment::CGeneric_Compartment(const sirael_lang::TSirael_Id id, std::vector<double>::const_iterator initial_quantities_begin, std::vector<double>::const_iterator initial_quantities_end, IOffset_Descriptor* descriptor) :
	mId(id), mSubstances{ initial_quantities_begin, initial_quantities_end }, mOffset_Descriptor(descriptor) {
}

sirael_lang::TSirael_Id CGeneric_Compartment::id() {
	return mId;
}


double CGeneric_Compartment::level(const sirael_lang::TSirael_Id substance) {
	if (substance == sirael_lang::dev_null_id)
		return 0.0;
	return (substance < mSubstances.size()) ? mSubstances[substance] : std::numeric_limits<double>::quiet_NaN();
}

bool CGeneric_Compartment::change_level(const sirael_lang::TSirael_Id substance, const double amount) {
	const double flushed = Subnormal_to_Zero(amount);

	if ((substance < mSubstances.size()) && Is_Positive(flushed)) {
		//is it a sensory signal or a quantity that we accumulate?
		const auto desc = mOffset_Descriptor->Describe(mId, substance);

		if (!desc.sensor)
			mSubstances[substance] += flushed;
		else
			mSubstances[substance] = flushed;	//no accumulation for the sensor! just copy the value

		return true;
	}
	else
		return false;
}

bool CGeneric_Compartment::passive_transport(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) {
	const double dst_level = dst->level(substance);	
	if (std::isnan(dst_level) || (substance >= mSubstances.size()))
		return false;

	double &src_level = mSubstances[substance];
	const double half_gradient_factor = dst->id() == sirael_lang::dev_null_id ? 1.0 : 0.5;
	const double half_raw_gradient = half_gradient_factor *(src_level - dst_level);	

	bool success = true;		//we check physiological plausibility, not the gradient's sign!
	if (half_raw_gradient > 0.0) {	
		const double substance_to_diffuse = Reacting_Product(half_raw_gradient, params.transport.p, params.transport.max_rate, dt);
					
		const auto desc = mOffset_Descriptor->Describe(mId, substance);	//food, injected insulin, these all have to be converted when getting out of the (any-complexity) depot

		success = dst->change_level(substance, substance_to_diffuse);
		if (success && !desc.sensor) {			
			src_level = Subnormal_to_Zero(src_level - substance_to_diffuse);			
		}
	}

	return success;
}


bool CGeneric_Compartment::equalize(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) {
	return passive_transport(substance, dst, params, dt) && dst->passive_transport(substance, this, params, dt);
}

bool CGeneric_Compartment::active_transport(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) {
	const double dst_level = dst->level(substance);
	if (std::isnan(dst_level) || (substance >= mSubstances.size()))
		return false;

	double& src_level = mSubstances[substance];
	const double gradient_factor =1.0;	//active transport do not follow the concentration gradient!
	
	if (src_level > 0.0) {
		const auto desc = mOffset_Descriptor->Describe(mId, substance);	//food, injected insulin, these all have to be converted when getting out of the (any-complexity) depot

		const double substance_to_transport = Reacting_Product(src_level * gradient_factor, params.transport.p, params.transport.max_rate, dt);		

		if (!desc.sensor) {
			src_level -= substance_to_transport;	//we cannot e.g.; decrease the ambient temperature
		}

		return dst->change_level(substance, substance_to_transport);
	} else
		return true;
}


bool CGeneric_Compartment::threshold_transport(const sirael_lang::TSirael_Id substance, CAbstract_Compartment* dst, const sirael_lang::TParameters& params, const double dt) {
	const double dst_level = dst->level(substance);
	if (std::isnan(dst_level) || (substance >= mSubstances.size()))
		return false;

	double& src_level = mSubstances[substance];
	const double half_gradient_factor = dst->id() == sirael_lang::dev_null_id ? 1.0 : 0.5;
	const double half_raw_gradient = half_gradient_factor * (src_level - dst_level);

	bool success = true;		//we check physiological plausibility, not the gradient's sign!
	if (half_raw_gradient > 0.0) {
		const double reacting_potential = std::max(0.0, (half_raw_gradient - params.inverse_transport.p) / half_raw_gradient);
		const double substance_to_diffuse = Reacting_Product_By_Potential(src_level, reacting_potential, params.inverse_transport.max_rate, dt);					

		const auto desc = mOffset_Descriptor->Describe(mId, substance);	//food, injected insulin, these all have to be converted when getting out of the (any-complexity) depot

		success = dst->change_level(substance, substance_to_diffuse);
		if (success && !desc.sensor) {
			src_level -= substance_to_diffuse;
		}
	}

	return success;
}


bool CGeneric_Compartment::react(sirael_lang::TSirael_Id reactant, sirael_lang::TSirael_Id moderator, sirael_lang::TSirael_Id product, const sirael_lang::TParameters& params, const double dt) {
	const auto& reaction = params.reaction;

	if ((reactant >= mSubstances.size()) || (reaction.ratio <= 0.0))
		return false;

	const bool single_substance = moderator == sirael_lang::dev_null_id;
	if ((!single_substance) && (moderator >= mSubstances.size()))
		return false;
	

	double &reactant_volume = mSubstances[reactant];
	
	double &moderator_volume = mSubstances[single_substance ? 0 : moderator];

	double reacting_base = Reacting_Product(reactant_volume, reaction.p, reaction.max_rate, dt);
	double moderating_base = 0.0;

	if (!single_substance) {	//if needed, correct the mas by the real moderator availability
		const double max_reactant_mass_as_permitted_by_moderator = moderator_volume / reaction.ratio;			
		if (reacting_base > max_reactant_mass_as_permitted_by_moderator)
			reacting_base = max_reactant_mass_as_permitted_by_moderator;

		moderating_base = std::min(moderator_volume, reacting_base * reaction.ratio);	//round-off errors, which could lead to negative values
	}
		
	reactant_volume -= reacting_base;
	if (!single_substance) {
		const auto desc = mOffset_Descriptor->Describe(mId, moderator);
		if (!desc.sensor) {
			moderator_volume = Subnormal_to_Zero(moderator_volume - moderating_base);
		}
	}
	
	if (product != sirael_lang::dev_null_id) {
		if (product >= mSubstances.size())
			return false;
		mSubstances[product] += reaction.product_fraction * (reacting_base + moderating_base);
	}

	return true;
}

bool CGeneric_Compartment::inverse_react(sirael_lang::TSirael_Id detector, sirael_lang::TSirael_Id moderator, sirael_lang::TSirael_Id product, const sirael_lang::TParameters& params, const double dt) {
	if ((detector >= mSubstances.size()) || (moderator >= mSubstances.size()) || (params.inverse_reaction.ratio <= 0.0))
		return false;		//note that dev null id as not allowed and it is greater the mSubstances.size() so we test it too

	const auto& inverse_reaction = params.inverse_reaction;

	const double detector_volume = mSubstances[detector];
	double& moderator_volume = mSubstances[moderator];

	const double max_acting_product = dt*std::max(0.0, inverse_reaction.max_rate - std::pow(inverse_reaction.p, detector_volume));
	const double available_product_by_moderator = moderator_volume / inverse_reaction.ratio;
	const double effective_acting_product = std::min(max_acting_product, available_product_by_moderator);
	const double used_moderator = std::min(moderator_volume, effective_acting_product * inverse_reaction.ratio);
							
	const auto desc = mOffset_Descriptor->Describe(mId, moderator);
	if (desc.sensor)
		return false;	//this has to change as this substance makes the product

	moderator_volume -= used_moderator;
	mSubstances[product] += effective_acting_product;

	
	return true;
}

bool CGeneric_Compartment::inhibit(sirael_lang::TSirael_Id moderator, sirael_lang::TSirael_Id inhibitor, sirael_lang::TSirael_Id product, const sirael_lang::TParameters& params, const double dt) {
	const auto &inhibition = params.inhibition;

	if ((moderator >= mSubstances.size()) || (inhibitor >= mSubstances.size()) || (inhibition.ratio<=0.0))
		return false;		

	double& moderator_volume = mSubstances[moderator];

	
	if (moderator_volume > 0.0) {
		
		const double& inhibitor_volume = mSubstances[inhibitor];
		
		const double reacting_base = moderator_volume - inhibitor_volume*inhibition.ratio;

		if (reacting_base > 0.0) {
			const double acting_product = Reacting_Product(reacting_base, inhibition.p, inhibition.max_rate, dt);
				
			auto final_level = mSubstances[product] + inhibition.product_fraction * acting_product;
			const auto cls = std::fpclassify(final_level);
			if (cls == FP_SUBNORMAL)
				final_level = 0.0;
			mSubstances[product] = final_level;
			//mSubstances[product] += inhibition.product_fraction * acting_product;
			moderator_volume -= acting_product;												
		}
	}

	return true;
};