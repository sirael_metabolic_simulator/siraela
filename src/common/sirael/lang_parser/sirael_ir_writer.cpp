/**
 * Sirael Language Compiler
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */


#include "sirael_ir_writer.h"

#include <iostream>
#include <fstream>

template <typename O>
bool Write_Program_IR_Core(O &output, const sirael_lang::TSirael_Program & in_mem_program, const std::string & banner, const bool save_names) {	
	sirael_ir::THeader header {};

	memcpy(header.magic, sirael_ir::magic, sizeof(sirael_ir::TMagic));	
	header.declared_compartment_count = in_mem_program.declared_compartment_count;
	header.declared_substances_count = in_mem_program.declared_substances_count;
	header.usecs_stepping = in_mem_program.usecs_stepping;
	header.signal_count = in_mem_program.signals.size();
	header.instruction_count = in_mem_program.program.size();
	header.state_count = in_mem_program.states.size();
	
	header.compartment_names_offset = header.substances_names_offset = 0;	//not implemented yet

	
	auto write_string = [&output](const std::string& str) {
		const uint64_t len = str.size();
		output.write(reinterpret_cast<const char*>(&len), sizeof(len));
		output.write(str.data(), len);
	};


	output.write(reinterpret_cast<const char*>(&header), sizeof(header));	//write preliminary header - we'll update its offset later
	
	header.signals_offset = output.tellp();
	for (size_t i = 0; i < header.signal_count; i++)
		output.write(reinterpret_cast<const char*>(&in_mem_program.signals[i]), sizeof(sirael_lang::TSignal));
		
	header.program_offset = output.tellp();
	for (size_t i = 0; i < header.instruction_count; i++)
		output.write(reinterpret_cast<const char*>(&in_mem_program.program[i]), sizeof(sirael_lang::TInstruction));
	
	header.states_offsets = output.tellp();
	for (size_t i = 0; i < header.state_count; i++) 
		output.write(reinterpret_cast<const char*>(in_mem_program.states[i].data()), sizeof(double) * header.declared_compartment_count * header.declared_substances_count);		

	header.banner_offset = output.tellp();
	write_string(banner);

	if (save_names) {
		header.compartment_names_offset = output.tellp();
		for (size_t i = 0; i < in_mem_program.compartment_names.size(); i++)
			write_string(in_mem_program.compartment_names[i]);

		header.substances_names_offset = output.tellp();
		for (size_t i = 0; i < in_mem_program.substance_names.size(); i++)
			write_string(in_mem_program.substance_names[i]);
	}

	//update the header and states offsets to finalize
	output.seekp(0);
	output.write(reinterpret_cast<const char*>(&header), sizeof(header));
	
	//and we are done	
	return true;
}


bool Write_Program_IR(const filesystem::path out_file, const sirael_lang::TSirael_Program& in_mem_program, const std::string& banner, const bool save_names) {
	std::ofstream output{ out_file, std::ios_base::binary | std::ios::trunc };
	if (output.is_open()) {
		return Write_Program_IR_Core(output, in_mem_program, banner, save_names);
	}
	else {
		std::cerr << "Cannot open output file " << out_file.string() << std::endl;
		return false;
	}

	//never should get here
	return false;
}


bool Write_Program_IR(std::ostream& output, const sirael_lang::TSirael_Program& in_mem_program, const std::string& banner, const bool save_names) {
	return Write_Program_IR_Core(output, in_mem_program, banner, save_names);
}
