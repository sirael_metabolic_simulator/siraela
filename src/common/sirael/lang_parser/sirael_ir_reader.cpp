/**
 * Sirael Language Compiler
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */

#include "sirael_ir_reader.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <optional>

template <typename T, typename V = std::vector<T>>
bool Read_Sequence(std::istream& input, V& container, const uint64_t offset, const size_t count, std::string &error_msg, const std::string &T_desc) {
	bool result = false;

	container.resize(count);
	if (input.seekg(offset)) {

		if (input.read(reinterpret_cast<char*>(container.data()), count * sizeof(T))) {
			result = true;
		}
		else
			error_msg = "Cannot read " + T_desc+ "!";
	}
	else
		error_msg = "Cannot seek to " + T_desc + "!";

	return result;
}



TLoaded_IR Read_Program_IR_Core(std::istream &input) {
	TLoaded_IR result;
	result.valid = false;
		
	sirael_ir::THeader header {};

	if (input.read(reinterpret_cast<char*>(&header), sizeof(header))) {
		if (memcmp(header.magic, sirael_ir::magic, sizeof(sirael_ir::TMagic)) == 0) {

			result.program.declared_compartment_count = header.declared_compartment_count;
			result.program.declared_substances_count = header.declared_substances_count;
				

			if (header.usecs_stepping > 0) {

				result.program.usecs_stepping = header.usecs_stepping;
				if (Read_Sequence<sirael_lang::TSignal>(input, result.program.signals, header.signals_offset, header.signal_count, result.error_msg, "signals")) {
					if (Read_Sequence<sirael_lang::TInstruction>(input, result.program.program, header.program_offset, header.instruction_count, result.error_msg, "program")) {


						result.program.states.resize(header.state_count);
						result.valid = true;
						if (input.seekg(header.states_offsets)) {
							for (size_t i = 0; i < header.state_count; i++) {
								if (!Read_Sequence<double>(input, result.program.states[i], input.tellg(), static_cast<size_t>(header.declared_compartment_count) * static_cast<size_t>(header.declared_substances_count), result.error_msg, std::to_string(i) + "-th state")) {
									result.valid = false;
									break;
								}
							}

							auto read_string = [&input, &result](const std::string& err_name) -> std::optional<std::string> {
								uint64_t len = 0;
								input.read(reinterpret_cast<char*>(&len), sizeof(len));
								std::string str;

								if (Read_Sequence<char, std::string>(input, str, input.tellg(), len, result.error_msg, err_name))
									return str;
								else
									return std::nullopt;
								};


							if (result.valid && (input.seekg(header.banner_offset))) {
								const auto val = read_string("banner");
								result.valid = val.has_value();
								if (result.valid)
									result.banner = val.value();
							}
							else {
								result.error_msg = "Cannot seek to banner!";
								result.valid = false;
							}

							auto read_string_vector = [&read_string](const size_t n, std::vector<std::string>& dst, const std::string& err_name)->bool {

								for (size_t i = 0; i < n; i++) {
									const auto val = read_string(err_name + std::to_string(i));
									if (!val.has_value())
										return false;
									dst.push_back(val.value());
								}
								return true;
								};


							if (result.valid && (header.compartment_names_offset > 0)) {
								if (input.seekg(header.compartment_names_offset))
									result.valid = read_string_vector(header.declared_compartment_count, result.program.compartment_names, "compartment id: ");
								else {
									result.error_msg = "Cannot seek to compartment names!";
									result.valid = false;
								}
							}

							if (result.valid && (header.substances_names_offset > 0)) {
								if (input.seekg(header.substances_names_offset))
									result.valid = read_string_vector(header.declared_substances_count, result.program.substance_names, "substance id: ");
								else {
									result.error_msg = "Cannot seek to substance names!";
									result.valid = false;
								}
							}

						}
						else
							result.error_msg = "Cannot seek to states!";

					}
				}

			}
			else
				result.error_msg = "Zero stepping would not advance the model!";
				

		} else
			result.error_msg = "Invalid IR file signature.";
	}
	else
		result.error_msg = "Cannot read from the IR!";


	return result;
}


TLoaded_IR Read_Program_IR(const filesystem::path ir_file) {
	TLoaded_IR result;
	result.valid = false;
	std::ifstream input(ir_file, std::ios::binary);
	if (input.is_open()) {
		result = Read_Program_IR_Core(input);
	}
	else {
		result.error_msg = "Cannot open the file " + ir_file.string();
	}
	
	return result;
}

TLoaded_IR Read_Program_IR(const void* IR_buffer, const size_t buffer_size) {
	std::istringstream input;
	input.rdbuf()->pubsetbuf(const_cast<char*>(static_cast<const char*>(IR_buffer)), buffer_size);
	return Read_Program_IR_Core(input);
}