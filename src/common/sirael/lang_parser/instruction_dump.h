/**
 * Sirael Language Compiler
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */

template <typename CN, typename SN>
std::string Dump_TInstruction_Lang(const sirael_lang::TInstruction& instr, const size_t substances_count, CN comp_name, SN sub_name) {

	std::stringstream result;

	auto decode_coord = [substances_count](const sirael_lang::TSirael_Id id)->sirael_lang::TSplit_Id {
		if (id == sirael_lang::dev_null_id)
			return { sirael_lang::dev_null_id, sirael_lang::dev_null_id };
		return sirael_lang::split_id(id, substances_count);		
	};

	auto encode_passive_transport = [&]() {
		const auto coord1 = decode_coord(instr.src1);
		const auto coord2 = decode_coord(instr.src2);

		result << "       transport: "
			<< sub_name(coord1.substance_id) << " in "
			<< comp_name(coord1.compartment_id) << " -> "
			<< comp_name(coord2.compartment_id);

		result << "\t(.p=" << instr.parameters.transport.p << " .max_rate=" << instr.parameters.transport.max_rate << ")";
		};

	auto encode_threshold_transport = [&]() {
		const auto coord1 = decode_coord(instr.src1);
		const auto coord2 = decode_coord(instr.src2);

		result << "threshold transport: "
			<< sub_name(coord1.substance_id) << " in "
			<< comp_name(coord1.compartment_id) << " -> "
			<< comp_name(coord2.compartment_id);

		result << "\t(.p=" << instr.parameters.transport.p << " .max_rate=" << instr.parameters.transport.max_rate << ")";
		};

	auto encode_equalize = [&]() {
		const auto coord1 = decode_coord(instr.src1);
		const auto coord2 = decode_coord(instr.src2);

		result << "        equalize: "
			<< sub_name(coord1.substance_id) << " in "
			<< comp_name(coord1.compartment_id) << " <-> "
			<< comp_name(coord2.compartment_id);

		result << "\t(.p=" << instr.parameters.transport.p << " .max_rate=" << instr.parameters.transport.max_rate << ")";
		};

	auto encode_active_transport = [&]() {
		const auto coord1 = decode_coord(instr.src1);
		const auto coord2 = decode_coord(instr.src2);

		result << "active transport: "
			<< sub_name(coord1.substance_id) << " in "
			<< comp_name(coord1.compartment_id) << " -> "
			<< comp_name(coord2.compartment_id);

		result << "\t(.p=" << instr.parameters.transport.p << " .max_rate=" << instr.parameters.transport.max_rate << ")";
		};


	auto encode_react = [&]() {
		const auto coord1 = decode_coord(instr.src1);
		const auto coord2 = decode_coord(instr.src2);
		const auto coordp = decode_coord(instr.product);

		result << "           react: "
			<< sub_name(coord1.substance_id);

		if (instr.src2 != sirael_lang::dev_null_id)
			result << " & " << sub_name(coord2.substance_id);

		result << " -> " << sub_name(coordp.substance_id)
			<< " in " << comp_name(coord1.compartment_id);	//the very first substances gives the compartment, where the reaction happens

		result << "\t(.p=" << instr.parameters.reaction.p << " .max_rate=" << instr.parameters.reaction.max_rate;
		if (instr.src2 != sirael_lang::dev_null_id)
			result << " .ratio=" << instr.parameters.reaction.ratio;
		result << " .product_fraction=" << instr.parameters.reaction.product_fraction << ")";
		};

	auto encode_inhibit = [&]() {
		const auto coord1 = decode_coord(instr.src1);
		const auto coord2 = decode_coord(instr.src2);
		const auto coordp = decode_coord(instr.product);

		result << " inhibited react: "
			<< sub_name(coord1.substance_id)
			<< " by " << sub_name(coord2.substance_id);

		result << " -> " << sub_name(coordp.substance_id)
			<< " in " << comp_name(coord1.compartment_id);

		result << "\t(.p=" << instr.parameters.inhibition.p << " .max_rate=" << instr.parameters.inhibition.max_rate <<
			" .ratio=" << instr.parameters.inhibition.ratio << " .product_fraction=" << instr.parameters.inhibition.product_fraction << ")";
		};

	auto encode_inverse_react = [&]() {
		const auto coord1 = decode_coord(instr.src1);
		const auto coord2 = decode_coord(instr.src2);
		const auto coordp = decode_coord(instr.product);

		result << "   inverse react: "
			<< sub_name(coord1.substance_id)
			<< " & " << sub_name(coord2.substance_id);

		result << " -> " << sub_name(coordp.substance_id)
			<< " in " << comp_name(coord1.compartment_id);

		result << "\t(.p=" << instr.parameters.inverse_reaction.p << " .max_rate=" << instr.parameters.inverse_reaction.max_rate << " .ratio=" << instr.parameters.inverse_reaction.ratio << ")";
		};


	switch (instr.opcode) {
		case sirael_lang::NInstruction::passive_transport:
			encode_passive_transport();
			break;

		case sirael_lang::NInstruction::equalize:
			encode_equalize();
			break;

		case sirael_lang::NInstruction::threshold_transport:
			encode_threshold_transport();
			break;			

		case sirael_lang::NInstruction::active_transport:
			encode_active_transport();
			break;

		case sirael_lang::NInstruction::react:
			encode_react();
			break;

		case sirael_lang::NInstruction::inhibit:
			encode_inhibit();
			break;

		case sirael_lang::NInstruction::inverse_react:
			encode_inverse_react();
			break;

		case sirael_lang::NInstruction::nop:
			result << "nop //no-operation instruction";
			break;

		case sirael_lang::NInstruction::end:
			result << "//end of program";
			break;

		default:
			result << "Unexpected instruction opcode! Opcode: " << static_cast<size_t>(instr.opcode);
			break;
	}


	return result.str();
}

