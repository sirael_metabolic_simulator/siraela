/**
 * Sirael Language Compiler
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */

#pragma once

#include <string>
#include <map>
#include <array>
#include <vector>

#include <scgms/iface/DeviceIface.h>
#include <scgms/rtl/guid.h>



namespace sirael_lang {

	constexpr uint64_t usecs_default_stepping = 10 * 1000 * 1000;		//10 seconds in microseconds is the default stepping

	using TSirael_Id = uint32_t;
	constexpr TSirael_Id Invalid_Id = std::numeric_limits<TSirael_Id>::max();
	constexpr TSirael_Id dev_null_id = Invalid_Id - 1;
	
	struct TSplit_Id {
		TSirael_Id compartment_id = Invalid_Id, substance_id = Invalid_Id;
	};

	TSirael_Id combine_ids(const TSirael_Id compartment_id, const TSirael_Id substance_id, const size_t substances_count);
	TSplit_Id split_id(const TSirael_Id id, const size_t substances_count);
	
	bool Valid_Id(const TSirael_Id id);
	bool Valid_Id(const TSplit_Id id);
	template <typename... Args>
	bool Valid_Id(const TSirael_Id id, Args... args) {
		return Valid_Id(id) && Valid_Id(args...);
	}


	struct TTransport {	//used for diffusion, equalize, active transport and depot diffusion, which takes signal's conversion rate attribute
		double p;	//y = gradient > 0.0 ? 2.0*0.5^(1-p/gradient) : 0;
					//gradient - to allow one way diffusion only
		double max_rate;	//see Michaelis-Menten Vm
	};


	struct TThreshold_Transport {	//a transport that occurs from a threshold given by p
		double p;			//(gradient-p)/gradient
		double max_rate;
	};
	
	struct TReaction {
		//order of the members is important to enable coalesced memory access
		double p;			//see the directed diffusion's p-parameter
		double max_rate;
		double ratio;			//reactant requires ratio*reactant volume of moderator to react
		double product_fraction;//e.g.; in glycolysis, when glucose and insulin in aerobic condition produce energy and lactate, 
		//we need the fraction of the both reactants' molar mass to express the produced lactate (or pyruvate in anaerobic)
	};

	struct TInverse_Reaction {
		double p;			//single volume, scaled reaction	with reaction_potential	= p - ratio^mass
		double max_rate;
		double ratio;
	};

	struct TInhibition {
		double p;//see the directed diffusion's p-parameter
		double max_rate;
		double ratio;					//degree of product suppression: moderator/(moderator+ratio*inhibitor)
		double product_fraction;		//see TReaction
	};


	constexpr size_t TParameters_Double_Count = std::max(sizeof(TTransport),
				std::max(sizeof(TReaction),
					std::max(sizeof(TInverse_Reaction), sizeof(TInhibition))))/ sizeof(double);
	enum class NInstruction_Parameter : size_t {
		p = 0,
		max_rate = 1,
		ratio = 2,
		fraction = 3,

		count,		
	};

	static_assert(static_cast<size_t>(NInstruction_Parameter::count) == TParameters_Double_Count);

	union TParameters {
		TReaction reaction;
		TInverse_Reaction inverse_reaction;
		TInhibition inhibition;
		TTransport transport;
		TThreshold_Transport inverse_transport;
		std::array<double, TParameters_Double_Count> vector {{}};
	};


	enum class NInstruction : TSirael_Id {
		passive_transport = 0,	//e.g.; one-way diffuse, e.g.;routing substances through the lymphatic system
		active_transport,	//e.g.; insulin from insulin depot or e.g.; sodium ions transporting glucose from gut to blood against the concentration gradient
		threshold_transport,	//e.g.; urination when exceeding a particular threshold of a glucose in the urine
		equalize,			//e.g.; glucose from blood to ist and back		

		react,			//e.g.; glucose and insulin produce ATP and pyruvate/lactate
		inhibit,		//e.g.; glucagon from current metabolic need, inhibited by current glucose level
		inverse_react,	//e.g.; set point acting on a low level

		nop,			//just to aid approaches such as genetic programming
		end,			//end of program
		count,
		invalid_encoding = Invalid_Id
	};

#pragma pack(push, 1)
	struct TInstruction {
		NInstruction opcode = NInstruction::invalid_encoding;
		TSirael_Id src1 = dev_null_id, src2 = dev_null_id, product = dev_null_id;		//TSirael_Id encodes both - compartment and substances, i.e.; these are offsets to the state
		TParameters parameters{ {} };
	};
#pragma pack(pop)

	enum class NSignal_Flag : TSirael_Id {
		none = 0,		
		sensor = 1,		//read-only signal whose value we copy-in, e.g., ambient temperature
		consume = 2,	//read-only signal, whose value we accumulate, e.g., consumed carbohydrates
		emit = 4		
	};

	inline NSignal_Flag& operator|=(NSignal_Flag &a, const NSignal_Flag b) noexcept {
		a = static_cast<NSignal_Flag>(static_cast<TSirael_Id>(a) | static_cast<TSirael_Id>(b));
		return a;
	}

	inline bool operator&&(NSignal_Flag a, const NSignal_Flag b) noexcept {		
		return (static_cast<TSirael_Id>(a) & static_cast<TSirael_Id>(b)) != 0;
	}

#pragma pack(push, 1)
	struct TSignal {
		GUID signal_id = Invalid_GUID;
		NSignal_Flag flags = NSignal_Flag::none;
		TSirael_Id substance_offset = dev_null_id;		//encodes substance and compartment
		double conversion_ratio = 1.0;
	};

	struct TString {
		uint64_t length = 0;
		//ansi chars follow, no ASCIIZ intended!
	};
#pragma pack(pop)

	struct TSirael_Program {
		TSirael_Id declared_compartment_count = 0;	//not counting dev_null
		TSirael_Id declared_substances_count = 0;	//not counting dev_null
		uint64_t usecs_stepping = usecs_default_stepping;
		std::vector<TSignal> signals;
		std::vector<TInstruction> program;
		std::vector<std::vector<double>> states;

		std::vector<std::string> substance_names, compartment_names;

		operator bool() const {
			return !program.empty() && //!states.empty() && states can be empty if we just encode the program
				!signals.empty() && (usecs_stepping > 0) &&
				(declared_compartment_count > 0) &&
				(declared_substances_count > 0);
		}
	};

}


namespace sirael_ir {
	

#pragma pack(push, 1)

	using TMagic = char[15];
	const TMagic magic = "Sirael IR v1.0";

	struct THeader {
		TMagic magic;
		sirael_lang::TSirael_Id declared_compartment_count = 0;	//not counting dev_null
		sirael_lang::TSirael_Id declared_substances_count = 0;	//not counting dev_null
		uint64_t usecs_stepping = 10 * 1000 * 1000;

		uint64_t banner_offset = 0;

		uint64_t compartment_names_offset = 0, substances_names_offset = 0;	//non-zero if present
		uint64_t signals_offset = 0, signal_count = 0, program_offset = 0, instruction_count = 0;
		uint64_t states_offsets = 0, state_count = 0;
		
	};
#pragma pack(pop)

}