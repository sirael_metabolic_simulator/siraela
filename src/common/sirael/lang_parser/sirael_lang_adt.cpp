/**
 * Sirael Language Compiler
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */

#include "sirael_lang_adt.h"

namespace sirael_lang {
	TSirael_Id combine_ids(const TSirael_Id compartment_id, const TSirael_Id substance_id, const size_t substances_count) {
		if (Valid_Id(compartment_id, substance_id) && (substances_count > 0))
			return compartment_id * static_cast<TSirael_Id>(substances_count) + substance_id;
		else
			return Invalid_Id;
			
	}

	TSplit_Id split_id(const TSirael_Id id, const size_t substances_count) {
		if (Valid_Id(id) && (substances_count > 0)) {
			const lldiv_t qr = lldiv(id, substances_count);
			return TSplit_Id{ static_cast<TSirael_Id>(qr.quot), static_cast<TSirael_Id>(qr.rem) };
		}
		else
			return { Invalid_Id, Invalid_Id };
	}


	bool Valid_Id(const TSirael_Id id) {
		return id != Invalid_Id;
	}

	bool Valid_Id(const TSplit_Id id) {
		return Valid_Id(id.compartment_id, id.substance_id);
	}
}