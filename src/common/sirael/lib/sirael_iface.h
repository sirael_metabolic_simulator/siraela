/**
 * Sirael - in-silico patient model
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injury, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *	https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 */


#pragma once


 //The following function are supposed to serve as the raw bindings to various languages. 
 //Hence, they are all C-oriented.

#if defined(_WIN32)
	#define C_Calling __cdecl
	#include <guiddef.h>
#else
	#define C_Calling

	#include <stdint.h>
	#include <cstddef>

	#ifndef GUID_DEFINED
		typedef struct GUID {
			uint32_t Data1;
			uint16_t Data2;
			uint16_t Data3;
			uint8_t  Data4[8];
		} GUID;

		#define GUID_DEFINED
	#endif

#endif

#ifdef cplusplus	
	using TSirael_Handle = void*;
	const TSirael_Handle Invalid_Sirael_Handle = nullptr;
	const size_t Recommended_Sirael_String_Buffer_Size = 2048;
#else
	#include <stdint.h>
	#include <stdbool.h>

	typedef void* TSirael_Handle;
	#define Invalid_Sirael_Handle NULL
	#define Recommended_Sirael_String_Buffer_Size 2048
#endif

	//each method, which returns uint64_t, returns uint64_t::max on error

	//returns either nullptr or handle value
extern "C" TSirael_Handle C_Calling Load_Metabolism_From_File(const char* IR_file_path, char* const error_buffer, const size_t error_buffer_size);
extern "C" TSirael_Handle C_Calling Load_Metabolism_From_Memory(const void* IR_buffer, const size_t buffer_size, char* const error_buffer, const size_t error_buffer_size);
extern "C" uint64_t C_Calling Get_State_Count(TSirael_Handle handle);
	//returns the state-index selected 
extern "C" uint64_t C_Calling Select_Metabolism_State(TSirael_Handle handle, const uint64_t state_index, char* const error_buffer, const size_t error_buffer_size);

	//tries to execute a step and returns the number of simulated microseconds
extern "C" uint64_t C_Calling Step(TSirael_Handle handle, const uint64_t seconds);

	//tries to "consume a quantity" xor "read sensor value" identified with the GUID	
	//use {B5897BBD-1E32-408A-A0D5-C5BFECF447D9} as a special id to set insulin basal rate
		//Warning: Real pump does not deliver insulin continuously, but discretely as a sequence of small boluses.
		//This library provides least-effort simulation of a continuous delivery for initial experimentation.
		//For a serious research, please, provide a real-pump simulator, which would deliver boluses only.
extern "C" uint64_t C_Calling Try_Consume(TSirael_Handle handle, const GUID* id, double quantity);	//non-zero if consumed, zero if not/error

	//returns the number of sampled levels
extern "C" uint64_t C_Calling Sampled_Level_Count(TSirael_Handle handle);
	//retrieves all the sampled levels, the arrays must have the size returned by Sampled_Level_Count
	//returns the number of sampled levels - IR specifies the levels to be sampled 
extern "C" uint64_t C_Calling Sample_Levels(TSirael_Handle handle, GUID* const ids, double* levels);


	//returns zero on success, otherwise an error
extern "C" void C_Calling Free_Metabolism(TSirael_Handle handle);