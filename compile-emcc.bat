@echo Sirael - in-silico patient model 
@echo Copyright (c) since 2020 Tomas Koutny.
@echo[
@echo Contact:
@echo tomas@sirael.science
@echo Tomas Koutny
@echo[
@echo Purpose of this software:
@echo This software is a personal, research project of Tomas Koutny. It is strictly
@echo prohibited to use this software for diagnosis or treatment of any medical condition,
@echo without proving a safety for the intended use.
@echo[
@echo By using this software, you are responsible for:
@echo   a) implementing all safety requirements,
@echo   b) proving the safety for a particular purpose,
@echo   c) obtaining all regulatory approvals,
@echo   d) any arising harm.
@echo[
@echo Especially, a diabetic patient is warned that improper use of this software
@echo may result into severe injure, including death.
@echo[
@echo Licensing terms:
@echo This software is granted under the Apache 2.0 license for a non-commercial use.
@echo This software can be granted for a commercial use under a written agreement only.
@echo Always, you must include and display this header-comment when redistributing the software in any form.
@echo[
@echo When referring to this work, use the following citation:
@echo Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials ^& Devices (2024).
@echo Available at
@echo   https://link.springer.com/article/10.1007/s44174-024-00199-9
@echo   or https://doi.org/10.1007/s44174-024-00199-9
@echo   or git: https://gitlab.com/sirael_metabolic_simulator/
@echo[
@echo This software uses some Apache2.0-licensed source files of the SmartCGMS project:
@echo   * Tomas Koutny and Martin Ubl, "SmartCGMS as a Testbed for a Blood-Glucose Level Prediction and/or 
@echo   * Control Challenge with (an FDA-Accepted) Diabetic Patient Simulation", Procedia Computer Science,  
@echo   * Volume 177, pp. 354-362, 2020
@echo[
@echo This software uses the Lean Mean C++ Option Parser, Copyright (C) 2012-2017 Matthias S. Benkmann, with
@echo  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software, to deal in the Software without restriction, including
@echo  * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
@echo  * persons to whom the Software is furnished to do so, subject to the following conditions:
@echo  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. 
@echo[
@echo -----------------------------------------------------------------------------------------------------------------------
@echo[
@echo This script will build the Siraela compiler and terminal.
@echo[
@echo It assumes to have clang in the PATH variable and having emcc environment initialized.
pause

@echo Building...


@rem emcc -O2 -std=c++20 -I src/common src/common/sirael/vm/compartment.cpp src/common/sirael/vm/siraela_vm.cpp src/common/sirael/vm/term_cmd.cpp src/common/sirael/lang_parser/sirael_ir_reader.cpp src/common/sirael/lang_parser/sirael_lang_adt.cpp src/console/terminal.cpp src/console/main.cpp src/console/logo.cpp src/common/scgms/utils/string_utils.cpp 
@rem emcc -O2 -std=c++20 -I src/common src/sc/commands.cpp src/sc/decompiler.cpp  src/sc/license.cpp  src/sc/main.cpp src/sc/options.cpp  src/common/sirael/lang_parser/sirael_ir_writer.cpp src/sc/sirael_lang_parser.cpp src/common/sirael/lang_parser/sirael_ir_reader.cpp src/common/sirael/lang_parser/sirael_lang_adt.cpp src/common/scgms/utils/string_utils.cpp  src/common/scgms/rtl/FilesystemLib.cpp 
@emcc --no-entry -sSIDE_MODULE -O2 -std=c++20  -I src/common src/lib/core.cpp src/lib/sirael_iface.cpp src/common/sirael/vm/compartment.cpp src/common/sirael/vm/siraela_vm.cpp src/common/sirael/lang_parser/sirael_ir_reader.cpp src/common/sirael/lang_parser/sirael_lang_adt.cpp src/common/scgms/utils/string_utils.cpp  src/common/scgms/rtl/FilesystemLib.cpp  -o siraela_lib.wasm  


@echo Cleaning...
@del *.o

@echo Done.